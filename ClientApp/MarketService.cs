using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using DangerousSprinkler;

namespace DangerousSprinkler.ClientApp
{
    internal class MarketService : IDisposable
    {
        public MarketService(Uri serverAddress)
            => m_serverAddress = serverAddress;

        public event EventHandler<CommodityUpdateEventArgs> CommodityUpdateReceived;

        public async Task Initialise()
        {
            Console.WriteLine($"going to connect to {new Uri(m_serverAddress, "/api/markets")}");
            m_ws = new JSONWSClient(new Uri(m_serverAddress, "/api/markets"));
            m_ws.MessageReceived += OnMessageReceived;
            await m_ws.ConnectAsync();
        }

        private void OnMessageReceived(object sender, JSONWSClient.MessageReceivedEventArgs args)
        {
            var content = args.Content.ToObject<LiveCommodityUpdate>();
            Console.WriteLine($"got commodity update for {content.CommodityName} at {content.SystemName}/{content.StationName}");
            CommodityUpdateReceived?.Invoke(this, new CommodityUpdateEventArgs { Update = content });
        }

        public void Subscribe(LiveCommoditySubscriptionInfo info)
        {
            JObject message = new();
            message.Add("message", "subscribe");
            message.Add("content", JObject.FromObject(info));
            _ = m_ws.SendTextAsync(message.ToString(Formatting.None));
        }

        public void Unsubscribe(string commodity)
        {
            JObject message = new();
            message.Add("message", "unsubscribe");
            JObject content = new();
            content.Add("commodity_name", commodity);
            message.Add("content", content);
            _ = m_ws.SendTextAsync(message.ToString(Formatting.None));
        }

        public void Dispose()
        {
            m_ws?.Dispose();
            m_ws = null;
        }

        private Uri m_serverAddress;
        private JSONWSClient m_ws;
    }

    internal class CommodityUpdateEventArgs
    {
        public LiveCommodityUpdate Update { get; init; }
    }
}