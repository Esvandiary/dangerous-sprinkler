using System;
using System.Net.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Text;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using DangerousSprinkler.Extensions;

namespace DangerousSprinkler.ClientApp
{
    public class Program
    {
        [Inject]
        public NavigationManager Navigation { get; set; }

        public static async Task Main(string[] args)
        {
            var builder = WebAssemblyHostBuilder.CreateDefault(args);
            builder.RootComponents.Add<App>("#app");

            Uri baseURI = new(builder.HostEnvironment.BaseAddress);
            Uri wsBaseURI = baseURI.WithScheme(baseURI.Scheme == Uri.UriSchemeHttps ? "wss" : "ws");

            StaticData sdata = new(baseURI);
            await sdata.Initialise();
            builder.Services.AddSingleton<StaticData>(sdata);

            MarketService market = new(wsBaseURI);
            await market.Initialise();
            builder.Services.AddSingleton<MarketService>(market);

            await builder.Build().RunAsync();
        }
    }
}
