using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using DangerousSprinkler;
using Newtonsoft.Json;

namespace DangerousSprinkler.ClientApp
{
    internal class StaticData
    {
        public ReadOnlyCollection<CommodityEntry> Commodities { get; private set; }

        public StaticData(Uri serverAddress)
            => m_serverAddress = serverAddress;

        public async Task Initialise()
        {
            using (HttpClient http = new HttpClient())
            {
                try
                {
                    var req = await http.GetStringAsync(new Uri(m_serverAddress, "/data/commodities"));
                    Commodities = new(JsonConvert.DeserializeObject<CommodityEntry[]>(req));
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"EXCEPTION getting commodities: {ex.Message}");
                }
            }
        }

        private Uri m_serverAddress;
    }
}