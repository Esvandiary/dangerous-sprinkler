using System;

namespace DangerousSprinkler
{
    public class SymbolicNameAttribute : Attribute
    {
        public SymbolicNameAttribute(string symbolic, string localised)
        {
            SymbolicName = symbolic;
            LocalisedName = localised;
        }

        public string SymbolicName { get; private set; }
        public string LocalisedName { get; private set; }
    }
}