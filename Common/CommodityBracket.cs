namespace DangerousSprinkler
{
    public enum CommodityBracket
    {
        None = 0,
        Low = 1,
        Medium = 2,
        High = 3,
        TemporaryAvailability = 4
    }
}