using System;

namespace DangerousSprinkler
{
    public static class Coordinates
    {
        public static CoordinateConverter X { get; } = new CoordinateConverter(49985, 32);
        public static CoordinateConverter Y { get; } = new CoordinateConverter(40985, 32);
        public static CoordinateConverter Z { get; } = new CoordinateConverter(24105, 32);

        public class CoordinateConverter
        {
            internal CoordinateConverter(double offset, double conversion)
            {
                Offset = offset;
                Scale = conversion;
            }

            public double Offset { get; private set; }
            public double Scale { get; private set; }
            public double ToGalaxy(long input) => (double)(input / Scale) - Offset;
            public long ToInternal(double input) => (long)Math.Round((input + Offset) * Scale);

        }
    }
}