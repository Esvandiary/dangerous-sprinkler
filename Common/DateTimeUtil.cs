using System;
using System.Globalization;

namespace DangerousSprinkler
{
    public static class DateTimeUtil
    {
        public static DateTimeOffset? FromUnixTimeSeconds(long? time)
        {
            return time.HasValue ? DateTimeOffset.FromUnixTimeSeconds(time.Value) : null;
        }

        public static DateTimeOffset FromISO8601(string time)
        {
            return DateTimeOffset.Parse(time, CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal);
        }

        public static string ToISO8601(DateTimeOffset time)
        {
            return time.ToString("o");
        }
    }
}
