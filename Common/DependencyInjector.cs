using System;
using System.Linq;
using System.Reflection;

namespace DangerousSprinkler
{
    public static class DependencyInjector
    {
        // private static void EnsureArgsMatch

        private static Type FindType(string type)
        {
            foreach (var a in AppDomain.CurrentDomain.GetAssemblies())
            {
                var t = a.GetType(type);
                if (t != null)
                    return t;
            }
            return null;
        }

        public static object CreateObject(string typename, params object[] ctorargs)
        {
            var type = FindType(typename);
            if (type == null)
                throw new TypeLoadException($"could not find type {typename}");
            var argtypes = ctorargs.Select(t => t.GetType()).ToArray();
            var ctor = type.GetConstructor(argtypes);
            if (ctor == null)
                throw new TypeLoadException($"could not find constructor for {typename} with arg types {String.Join(",", argtypes.Select(t => t.FullName))}");
            return ctor.Invoke(ctorargs);
        }

        public static T CreateObject<T>(string typename, params object[] ctorargs) => (T)CreateObject(typename, ctorargs);
    }
}