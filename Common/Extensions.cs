using System;
using System.Collections.Generic;
using System.Linq;

namespace DangerousSprinkler.Extensions
{
    public static class DateTimeExtensions
    {
        public static DateTimeOffset ToDateTimeOffset(this DateTime d, TimeSpan tz)
            => new DateTimeOffset(d, tz);
    }

    public static class TimeSpanExtensions
    {
        public static string ToHumanReadableString(this TimeSpan t)
        {
            if (t.TotalMinutes < 1.0) {
                return $@"{t:%s}s";
            }
            if (t.TotalHours < 1.0) {
                return $@"{t:%m}m {t:%s}s";
            }
            if (t.TotalDays < 1.0) {
                return $@"{t:%h}h {t:%m}m";
            }

            return $@"{t:%d}d {t:%h}h";
        }
    }

    public static class DictionaryExtensions
    {
        public static void RemoveAll<TKey, TValue>(this IDictionary<TKey, TValue> dic,
            Func<TKey, TValue, bool> predicate)
        {
            var keys = dic.Keys.Where(k => predicate(k, dic[k])).ToList();
            foreach (var key in keys)
            {
                dic.Remove(key);
            }
        }
    }

    public static class UriExtensions
    {
        public static Uri WithScheme(this Uri uri, string scheme)
        {
            return new UriBuilder(uri)
            {
                Scheme = scheme,
                Port = uri.IsDefaultPort ? -1 : uri.Port
            }.Uri;
        }
    }
}