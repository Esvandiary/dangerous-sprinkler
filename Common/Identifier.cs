using System;
using System.Collections.Generic;
using System.Reflection;

namespace DangerousSprinkler
{
    public static class Identifier
    {
        public static T FromNumber<T>(int n) where T : struct, Enum
        {
            if (Enum.IsDefined(typeof(T), n))
                return (T)Enum.ToObject(typeof(T), n);
            else
                return Enum.Parse<T>("Unknown");
        }

        public static T FromSymbolicName<T>(string name) where T : struct, Enum
            => FromSymbolicNameOrDefault(name, Enum.Parse<T>("Unknown"));

        public static T FromSymbolicNameOrDefault<T>(string name, T defaultValue) where T : struct, Enum
        {
            if (!m_symbolic.ContainsKey(typeof(T)))
                m_symbolic[typeof(T)] = GenerateSymbolic(typeof(T));
            string nameLower = name?.ToLowerInvariant();
            if (name == null || !m_symbolic[typeof(T)].ContainsKey(nameLower))
                return defaultValue;
            return Enum.GetValues<T>()[m_symbolic[typeof(T)][nameLower]];
        }

        public static T FromLocalisedName<T>(string name) where T : struct, Enum
            => FromLocalisedNameOrDefault(name, Enum.Parse<T>("Unknown"));

        public static T FromLocalisedNameOrDefault<T>(string name, T defaultValue) where T : struct, Enum
        {
            if (!m_localised.ContainsKey(typeof(T)))
                m_localised[typeof(T)] = GenerateLocalised(typeof(T));
            string nameLower = name?.ToLowerInvariant();
            if (name == null || !m_localised[typeof(T)].ContainsKey(nameLower))
                return defaultValue;
            return Enum.GetValues<T>()[m_localised[typeof(T)][nameLower]];
        }

        public static string ToSymbolicName<T>(T value) where T : struct, Enum
        {
            if (!m_symbolicNames.ContainsKey(typeof(T)))
                m_symbolicNames[typeof(T)] = GenerateSymbolicNames(typeof(T));
            return m_symbolicNames[typeof(T)][(int)(object)value];
        }

        public static string ToLocalisedName<T>(T value) where T : struct, Enum
        {
            if (!m_localisedNames.ContainsKey(typeof(T)))
                m_localisedNames[typeof(T)] = GenerateLocalisedNames(typeof(T));
            return m_localisedNames[typeof(T)][(int)(object)value];
        }


        private static Dictionary<string, int> GenerateSymbolic(Type t)
            => Generate(t, (sai) => sai.SymbolicName.ToLowerInvariant());
        private static Dictionary<string, int> GenerateLocalised(Type t)
            => Generate(t, (sai) => sai.LocalisedName.ToLowerInvariant());
        private static Dictionary<string, int> Generate(Type t, Func<SymbolicNameAttribute, string> fn)
        {
            Dictionary<string, int> values = new Dictionary<string, int>();
            foreach (var field in t.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                var sai = field.GetCustomAttribute<SymbolicNameAttribute>();
                if (sai != null)
                    values[fn(sai)] = (int)field.GetRawConstantValue();
            }
            return values;
        }

        private static Dictionary<int, string> GenerateNames(Type t, Func<SymbolicNameAttribute, string> fn)
        {
            Dictionary<int, string> values = new Dictionary<int, string>();
            foreach (var field in t.GetFields(BindingFlags.Public | BindingFlags.Static))
            {
                var sai = field.GetCustomAttribute<SymbolicNameAttribute>();
                if (sai != null)
                    values[(int)field.GetRawConstantValue()] = fn(sai);
            }
            return values;
        }
        private static Dictionary<int, string> GenerateSymbolicNames(Type t)
            => GenerateNames(t, (sai) => sai.SymbolicName);
        private static Dictionary<int, string> GenerateLocalisedNames(Type t)
            => GenerateNames(t, (sai) => sai.LocalisedName);


        private static readonly Dictionary<Type, Dictionary<string, int>> m_symbolic
            = new Dictionary<Type, Dictionary<string, int>>();
        private static readonly Dictionary<Type, Dictionary<string, int>> m_localised
            = new Dictionary<Type, Dictionary<string, int>>();
        private static readonly Dictionary<Type, Dictionary<int, string>> m_symbolicNames
            = new Dictionary<Type, Dictionary<int, string>>();
        private static readonly Dictionary<Type, Dictionary<int, string>> m_localisedNames
            = new Dictionary<Type, Dictionary<int, string>>();
    }
}