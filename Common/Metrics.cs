using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.Extensions.ObjectPool;

namespace DangerousSprinkler
{
    public class Metrics
    {
        public Capturer Capture(string id)
        {
            if (!_captureHistory.ContainsKey(id)) _captureHistory[id] = new CaptureHistory(DefaultTimeHorizon);
            var c = new Capturer(this, id);
            c.Begin();
            return c;
        }

        public TimeSpan Mean(string id) => _captureHistory[id].GetMean();
        public TimeSpan Max(string id) => _captureHistory[id].GetMax();
        public TimeSpan Min(string id) => _captureHistory[id].GetMin();

        public TimeSpan DefaultTimeHorizon { get; set; } = TimeSpan.FromSeconds(60);

        private void SubmitCapture(string captureID, TimeSpan duration)
        {
            _captureHistory[captureID].Add(duration);
        }

        public struct Capturer : IDisposable
        {
            internal Capturer(Metrics parent, string captureID)
            {
                _parent = parent;
                _captureID = captureID;
                _stopwatch = parent._stopwatchCache.Get();
            }

            public void Begin()
            {
                _stopwatch.Start();
            }

            public void End()
            {
                _stopwatch.Stop();
                _parent.SubmitCapture(_captureID, _stopwatch.Elapsed);
            }

            public void Dispose()
            {
                End();
                _stopwatch.Reset();
                _parent._stopwatchCache.Return(_stopwatch);
            }

            private Metrics _parent;
            private string _captureID;
            private Stopwatch _stopwatch;
        }

        private class CaptureHistory
        {
            public CaptureHistory(TimeSpan timeHorizon)
            {
                TimeHorizon = timeHorizon;
                // Hack: make sure it's never empty
                Add(TimeSpan.Zero);
            }

            public void Add(TimeSpan duration)
            {
                lock (_entries)
                {
                    PruneHistory();
                    _lastAddedTime = DateTimeOffset.UtcNow;
                    _entries.Enqueue(new Entry { TimeAdded = _lastAddedTime, Duration = duration });
                }
            }

            private void PruneHistory()
            {
                DateTimeOffset limit = DateTimeOffset.UtcNow - TimeHorizon;
                lock (_entries)
                {
                    while (_entries.TryPeek(out Entry entry) && entry.TimeAdded < limit)
                        _entries.Dequeue();
                }
            }

            public TimeSpan GetMean()
            {
                lock (_entries)
                    return TimeSpan.FromTicks(_entries.Select(t => t.Duration.Ticks).Sum() / _entries.Count);
            }

            public TimeSpan GetMax() { lock (_entries) return _entries.Max(t => t.Duration); }

            public TimeSpan GetMin() { lock (_entries) return _entries.Min(t => t.Duration); }

            public TimeSpan GetMeanInterval()
            {
                lock (_entries)
                    return (_lastAddedTime - _entries.Peek().TimeAdded) / (_entries.Count - 1);
            }

            public double GetMeanRate() => 1.0 / GetMeanInterval().TotalSeconds;


            public TimeSpan TimeHorizon { get; private set; }

            private Queue<Entry> _entries = new Queue<Entry>(128);
            private DateTimeOffset _lastAddedTime;

            private struct Entry
            {
                public DateTimeOffset TimeAdded;
                public TimeSpan Duration;
            }
        }

        private Dictionary<string, CaptureHistory> _captureHistory
            = new Dictionary<string, CaptureHistory>();

        private ObjectPool<Stopwatch> _stopwatchCache
            = new DefaultObjectPool<Stopwatch>(new DefaultPooledObjectPolicy<Stopwatch>(), 64);
    }
}