namespace DangerousSprinkler
{
    public enum PadSize
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("S", "Small")]
        Small = 1,
        [SymbolicName("M", "Medium")]
        Medium = 2,
        [SymbolicName("L", "Large")]
        Large = 3,
    }

    public enum StationType
    {
        [SymbolicName("Unknown", "Unknown")]
        Unknown,
        [SymbolicName("Coriolis", "Coriolis Starport")]
        Coriolis,
        [SymbolicName("Orbis", "Orbis Starport")]
        Orbis,
        [SymbolicName("Ocellus", "Ocellus Starport")]
        Ocellus,
        [SymbolicName("AsteroidBase", "Asteroid Base")]
        AsteroidBase,
        [SymbolicName("MegaShip", "Mega Ship")]
        Megaship,
        [SymbolicName("FleetCarrier", "Fleet Carrier")]
        FleetCarrier,
        [SymbolicName("Outpost", "Outpost")]
        Outpost,
        [SymbolicName("PlanetaryPort", "Planetary Port")]
        PlanetaryPort,
        [SymbolicName("PlanetaryOutpost", "Planetary Outpost")]
        PlanetaryOutpost
    }

    public class StationInfo
    {
        public static StationInfo FromName(string name)
        {
            switch (name)
            {
            case "Outpost":
                return new StationInfo(StationType.Outpost);
            case "AsteroidBase":
            case "Asteroid base":
                return new StationInfo(StationType.AsteroidBase);
            case "Coriolis":
            case "Coriolis Starport":
                return new StationInfo(StationType.Coriolis);
            case "Bernal":
            case "Ocellus":
            case "Ocellus Starport":
                return new StationInfo(StationType.Ocellus);
            case "Orbis":
            case "Orbis Starport":
                return new StationInfo(StationType.Orbis);
            case "FleetCarrier":
            case "Fleet Carrier":
                return new StationInfo(StationType.FleetCarrier);
            case "MegaShip":
            case "MegaShipCivilian":
            case "Mega ship":
                return new StationInfo(StationType.Megaship);
            case "CraterOutpost":
            case "Planetary Outpost":
            case "SurfaceStation":
                return new StationInfo(StationType.PlanetaryOutpost);
            case "CraterPort":
            case "Planetary Port":
                return new StationInfo(StationType.PlanetaryPort);
            default:
                return null;
            }
        }

        public StationInfo(StationType type)
        {
            Type = type;
        }

        public StationType Type { get; init; }
        public string SymbolicName { get => Identifier.ToSymbolicName(Type); }
        public string LocalisedName { get => Identifier.ToLocalisedName(Type); }
        public PadSize MaxPadSize { get => (Type != StationType.Outpost) ? PadSize.Large : PadSize.Medium; }
        public bool IsPlanetary { get => (Type == StationType.PlanetaryOutpost || Type == StationType.PlanetaryPort); }
        public bool IsFleetCarrier { get => (Type == StationType.FleetCarrier); }
    }
}