using System;
using System.Buffers;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DangerousSprinkler
{
    public abstract class WSClient : IDisposable
    {
        public WSClient(Uri serverAddress) => m_serverAddress = serverAddress;

        public async Task ConnectAsync()
        {
            await m_ws.ConnectAsync(m_serverAddress, m_disposalSource.Token);
            _ = ReceiveLoop();
        }

        private async Task ReceiveLoop()
        {
            var buf = ArrayPool<byte>.Shared.Rent(8192);
            StringBuilder receivedText = new();
            while (!m_disposalSource.IsCancellationRequested)
            {
                WebSocketReceiveResult received = null;
                while (!m_disposalSource.IsCancellationRequested)
                {
                    received = await m_ws.ReceiveAsync(buf, m_disposalSource.Token);
                    if (received.CloseStatus.HasValue)
                    {
                        m_disposalSource.Cancel();
                        break;
                    }
                    receivedText.Append(Encoding.UTF8.GetString(buf, 0, received.Count));
                    if (received.EndOfMessage) break;
                }
                if (!m_disposalSource.IsCancellationRequested && receivedText.Length != 0)
                    HandleRawMessage(receivedText.ToString());
                receivedText.Clear();
            }
            ArrayPool<byte>.Shared.Return(buf);
        }

        public async Task SendTextAsync(string message)
        {
            var data = new ArraySegment<byte>(Encoding.UTF8.GetBytes(message));
            await m_ws.SendAsync(data, WebSocketMessageType.Text, true, m_disposalSource.Token);
        }

        public async Task CloseAsync()
        {
            if (!m_ws.CloseStatus.HasValue)
                await m_ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "Client exiting", m_disposalSource.Token);
        }

        public void Dispose()
        {
            m_disposalSource.Cancel();
            if (!m_ws.CloseStatus.HasValue)
                _ = m_ws.CloseAsync(WebSocketCloseStatus.NormalClosure, "Client exiting", CancellationToken.None);
        }

        protected abstract void HandleRawMessage(string message);

        private Uri m_serverAddress;
        private ClientWebSocket m_ws = new();
        private CancellationTokenSource m_disposalSource = new();
    }

    public class TextWSClient : WSClient
    {
        public TextWSClient(Uri addr) : base(addr) {}
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        protected override void HandleRawMessage(string message)
        {
            try
            {
                MessageReceived?.Invoke(this, new MessageReceivedEventArgs { Message = message });
            }
            catch (Exception)
            {
                // TODO: log somehow
            }
        }

        public class MessageReceivedEventArgs : EventArgs
        {
            public string Message { get; init; }
        }
    }
}