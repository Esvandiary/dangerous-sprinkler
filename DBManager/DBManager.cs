﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;

namespace DangerousSprinkler
{
    public partial class DBManager : SprinklerComponent, IDBDeriveParameters
    {
        public delegate void DBAccessFunction(DbConnection dbc, IDBDeriveParameters dbm);

        public DBManager(CoreServices cs)
            : base(cs, "DBManager", "Database Manager")
        {
            string driver = Config.Get<string>("db.driver");
            switch (driver)
            {
                case "postgres": m_backend = new PostgresDBBackend(Config); break;
                default: throw new ApplicationException($"unknown DB driver '{driver}' specified");
            }

            RegisterGenerateStep(GenerateDBTables);
        }

        public void RegisterGenerateStep(DBAccessFunction fn) => m_generateFunctions.Add(fn);
        public void RegisterPostGenerateStep(DBAccessFunction fn) => m_postGenerateFunctions.Add(fn);
        public void RegisterInitStep(DBAccessFunction fn) => m_initFunctions.Add(fn);

        public DbConnection CreateConnection()
        {
            return m_backend.CreateConnection();
        }

        public async Task<DbConnection> CreateConnectionAsync()
        {
            return await m_backend.CreateConnectionAsync();
        }

#region SprinklerComponent
        public override void Initialise()
        {
            base.Initialise();
            bool creating = Config.GetOrDefault<bool>("db.create", false);
            if (creating)
            {
                Logger.Info("Recreating database...");
                m_backend.CreateDB();
                Logger.Info("Done recreating database.");
            }

            m_adminConnection = CreateConnection();

            if (creating)
            {
                Logger.Info("Running database table generate functions...");
                foreach (var step in m_generateFunctions)
                    step(m_adminConnection, this);
                Logger.Info("Done running database table generate functions.");
            }
        }

        public override void Start()
        {
            bool creating = Config.GetOrDefault<bool>("db.create", false);
            if (creating)
            {
                Logger.Info("Running database table post-generate functions...");
                foreach (var step in m_postGenerateFunctions)
                    step(m_adminConnection, this);
                Logger.Info("Done running database table post-generate functions.");
            }

            base.Start();
        }

        public override void Stop()
        {
            base.Stop();
            m_adminConnection.Close();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
            {
                m_adminConnection?.Dispose();
                m_adminConnection = null;
            }
        }
#endregion

        public void DeriveParameters(DbCommand command)
        {
            m_backend.DeriveParameters(command);
        }

        private void GenerateDBTables(DbConnection dbc, IDBDeriveParameters dbm)
        {
            var cmd = dbc.CreateCommand();
            cmd.CommandText = DBTableSchemas.Components;
            cmd.ExecuteNonQuery();
        }

        private IDBBackend m_backend;
        private DbConnection m_adminConnection;
        private List<DBAccessFunction> m_generateFunctions = new List<DBAccessFunction>();
        private List<DBAccessFunction> m_postGenerateFunctions = new List<DBAccessFunction>();
        private List<DBAccessFunction> m_initFunctions = new List<DBAccessFunction>();
    }
}
