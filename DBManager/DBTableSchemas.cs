using System;

namespace DangerousSprinkler
{
    public partial class DBManager
    {
        private static class DBTableSchemas
        {
            public static readonly string Components = @"
                CREATE TABLE components (
                    id VARCHAR(32) PRIMARY KEY,
                    initialised_at TIMESTAMP NOT NULL,
                    updated_at TIMESTAMP
                )
            ";
        }
    }
}