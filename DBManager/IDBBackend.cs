using System.Data.Common;
using System.Threading.Tasks;

namespace DangerousSprinkler
{
    internal interface IDBBackend
    {
        DbConnection CreateConnection();
        Task<DbConnection> CreateConnectionAsync();
        void DeriveParameters(DbCommand command);
        void CreateDB();
    }
}