using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Threading.Tasks;
using Npgsql;

namespace DangerousSprinkler
{
    internal class PostgresDBBackend : IDBBackend
    {
        public PostgresDBBackend(Config config)
        {
            m_config = config;
            List<string> connectParts = new List<string>();
            connectParts.Add($"Host={config.Get<string>("db.host")}");
            connectParts.Add($"Database={config.Get<string>("db.db_name")}");
            if (config.Has("db.port"))
                connectParts.Add($"Port={config.Get<int>("db.port")}");
            if (config.Has("db.username"))
                connectParts.Add($"User Id={config.Get<string>("db.username")}");
            if (config.Has("db.password"))
                connectParts.Add($"Password={config.Get<string>("db.password")}");
            m_connectString = String.Join(';', connectParts);
        }

        public DbConnection CreateConnection()
        {
            var conn = new NpgsqlConnection(m_connectString);
            conn.Open();
            return conn;
        }

        public async Task<DbConnection> CreateConnectionAsync()
        {
            var conn = new NpgsqlConnection(m_connectString);
            await conn.OpenAsync();
            return conn;
        }

        public void DeriveParameters(DbCommand command)
        {
            NpgsqlCommandBuilder.DeriveParameters((NpgsqlCommand)command);
        }

        public void CreateDB()
        {
            List<string> connectParts = new List<string>();
            connectParts.Add($"Host={m_config.Get<string>("db.host")}");
            connectParts.Add($"Database=postgres");
            if (m_config.Has("db.port"))
                connectParts.Add($"Port={m_config.Get<int>("db.port")}");
            if (m_config.Has("db.create_user_username"))
                connectParts.Add($"User Id={m_config.Get<string>("db.create_user_username")}");
            if (m_config.Has("db.create_user_password"))
                connectParts.Add($"Password={m_config.Get<string>("db.create_user_password")}");
            using (DbConnection create_dbc = new NpgsqlConnection(String.Join(';', connectParts)))
            {
                create_dbc.Open();
                using (DbCommand drop_cmd = create_dbc.CreateCommand())
                {
                    if (m_config.Get<string>("db.db_name") == "postgres")
                        throw new ApplicationException("you tried to make me delete your Postgres admin database, I'm going to decline");
                    drop_cmd.CommandText = $"DROP DATABASE IF EXISTS {m_config.Get<string>("db.db_name")}";
                    drop_cmd.ExecuteNonQuery();
                }
                using (DbCommand create_cmd = create_dbc.CreateCommand())
                {
                    create_cmd.CommandText = $@"CREATE DATABASE {m_config.Get<string>("db.db_name")} ENCODING UTF8";
                    if (m_config.Has("db.username"))
                        create_cmd.CommandText += $" WITH OWNER {m_config.Get<string>("db.username")}";
                    create_cmd.ExecuteNonQuery();
                }
            }
        }


        private Config m_config;
        private string m_connectString;
    }
}