using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using DangerousSprinkler.Extensions;
using DangerousSprinkler.EDDN;


namespace DangerousSprinkler
{
    public class EDDNReceiver : ZMQReceiver
    {
        public EDDNReceiver(string host, int port, Logger logger) : base(host, port)
        {
            m_logger = logger;
        }

        protected override void OnMessageReceived(string message)
        {
            try
            {
                // parse
                JObject o = JObject.Parse(message);
                // get schema and add to queue(s)
                if (o.ContainsKey("$schemaRef"))
                {
                    string schema = o.Value<string>("$schemaRef");
                    if (m_queues.ContainsKey(schema))
                    {
                        // We likely have queues to send this to, so check to see if we have the sender blacklisted
                        // TODO: actually check
                        Header header = o["header"].ToObject<Header>();
                        foreach (var q in m_queues[schema])
                            q.Add(o);
                    }
                }
                else
                {
                    m_logger.Debug($"Bad message received without schema ref: {message}");
                }
            }
            catch (Exception ex)
            {
                m_logger.Error($"Exception in OnMessageReceived (invalid JSON?): {ex.Message}");
            }
        }

        public bool AddListener(IReceiverListener listener)
        {
            if (m_listeners.Contains(listener))
                return false;
            m_listeners.Add(listener);
            foreach (string schema in listener.SchemaRefs)
            {
                m_logger.Debug($"Adding listener for schema {schema}");
                if (!m_queues.ContainsKey(schema))
                    m_queues[schema] = new List<BlockingCollection<JObject>>();
                m_queues[schema].Add(listener.Queue);
            }
            return true;
        }

        public bool RemoveListener(IReceiverListener listener)
        {
            if (!m_listeners.Contains(listener))
                return false;
            foreach (var kv in m_queues)
                kv.Value.Remove(listener.Queue);
            m_queues.RemoveAll((k, v) => v.Count == 0);
            m_listeners.Remove(listener);
            return true;
        }

        
        private ISet<IReceiverListener> m_listeners = new HashSet<IReceiverListener>();
        private IDictionary<string, List<BlockingCollection<JObject>>> m_queues
            =  new Dictionary<string, List<BlockingCollection<JObject>>>();
        private Logger m_logger;
    }
}