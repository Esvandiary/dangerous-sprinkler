﻿using System;
using System.Text;
using System.Threading;
using NetMQ;
using NetMQ.Sockets;
using Ionic.Zlib;

namespace DangerousSprinkler
{
    public abstract class ZMQReceiver : IDisposable
    {
        protected abstract void OnMessageReceived(string message);

        public ZMQReceiver(string host, int port)
        {
            m_host = host;
            m_port = port;
        }

        public void Connect()
        {
            lock (m_readThreadLock)
            {
                ConnectZMQ();
                if (m_readThread == null)
                    m_readThread = new Thread(ReadFromZMQ);
                if (!m_readThread.IsAlive)
                    m_readThread.Start();
            }
        }

        private void ConnectZMQ()
        {
            if (m_socket == null)
                throw new ObjectDisposedException("m_socket");
            m_socket.Options.ReceiveHighWatermark = 1024;
            m_socket.Connect(EndpointAddress);
            m_socket.SubscribeToAnyTopic();
            IsRunning = true;
        }

        public void Disconnect()
        {
            lock (m_readThreadLock)
            {
                DisconnectZMQ();
                if (m_readThread != null && m_readThread.IsAlive)
                    m_readThread.Join();
                m_readThread = null;
            }
        }

        private void DisconnectZMQ()
        {
            if (m_socket == null)
                throw new ObjectDisposedException("m_socket");
            IsRunning = false;
            try
            {
                m_socket.Disconnect(EndpointAddress);
            }
            catch (Exception)
            {
                // ignore exception, assume we already disconnected
            }
        }

        private void ReadFromZMQ()
        {
            while (IsRunning)
            {
                try
                {
                    byte[] compressedBytes = null;
                    while (IsRunning && !m_socket.TryReceiveFrameBytes(ReceiveBlockDuration, out compressedBytes));
                    if (IsRunning)
                    {
                        if (compressedBytes != null)
                        {
                            var bytes = ZlibStream.UncompressBuffer(compressedBytes);
                            OnMessageReceived(Encoding.UTF8.GetString(bytes));
                        }
                    }
                    else
                    {
                        return;
                    }
                }
                catch (Exception)
                {
                    Disconnect();
                    if (AutoReconnect)
                    {
                        TimeSpan currentDelay = AutoReconnectDelay;
                        while (m_socket != null && !IsRunning)
                        {
                            double delay_ms = Math.Min(currentDelay.TotalMilliseconds, AutoReconnectDelayLimit.TotalMilliseconds);
                            Thread.Sleep((int)delay_ms);
                            try { Connect(); continue; } catch (Exception) {}
                            currentDelay *= AutoReconnectBackoffMultiplier;
                        }
                        continue;
                    }
                    else
                    {
                        return;
                    }
                }
            }
        }

#region IDisposable
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                Disconnect();
                m_socket?.Dispose();
                m_socket = null;
            }
        }

        ~ZMQReceiver()
        {
            Dispose(false);
        }
#endregion

        public string EndpointAddress { get { return $"tcp://{m_host}:{m_port}"; } }
        public bool IsRunning { get; private set; } = false;
        public bool AutoReconnect { get; set; } = true;
        public TimeSpan AutoReconnectDelay { get; set; } = new TimeSpan(0, 0, 1);
        public TimeSpan AutoReconnectDelayLimit { get; set; } = new TimeSpan(0, 5, 0);
        public double AutoReconnectBackoffMultiplier { get; set; } = 2;

        private SubscriberSocket m_socket = new SubscriberSocket();
        private Thread m_readThread;
        private object m_readThreadLock = new object();
        private string m_host;
        private int m_port;
        private static readonly TimeSpan ReceiveBlockDuration = TimeSpan.FromMilliseconds(250);
    }
}
