﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using DangerousSprinkler.Extensions;

namespace DangerousSprinkler
{
    public partial class BGSTracker : SprinklerComponent
    {
        public BGSTracker(CoreServices cs, DBManager db)
            : base(cs, "BGSTracker", "BGS Tracker")
        {
            var tracker_type = cs.Config.GetOrDefault("galaxy_info.tick_tracker_type", "DangerousSprinkler.PhelboreTickTracker");
            Ticks = DependencyInjector.CreateObject<ITickTracker>(tracker_type, GetSubLogger("TickTracker"));
            m_db = db;

            m_db.RegisterGenerateStep(GenerateDBTables);
            m_db.RegisterPostGenerateStep(GenerateDBIndexes);
        }

        public ITickTracker Ticks { get; private set; }

        public List<MinorFaction.SystemStatus> GetFactions(StarSystem s)
        {
            var statuses = new Dictionary<string, MinorFaction.SystemStatus>();
            using (var dbc = m_db.CreateConnection())
            {
                using (var sf_cmd = dbc.CreateCommand())
                {
                    sf_cmd.CommandText = @"
                        SELECT
                            name, f.updated_at AS f_updated_at,
                            allegiance, government, is_player_faction,
                            sf.updated_at AS sf_updated_at, influence, happiness
                        FROM factions f
                        INNER JOIN system_factions sf ON f.name = sf.faction
                        WHERE sf.system_id = @id64
                    ";
                    m_db.DeriveParameters(sf_cmd);
                    sf_cmd.Parameters["@id64"].Value = (long)s.ID;
                    using (var reader = sf_cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            MinorFaction f = new MinorFaction
                            {
                                Name = reader.GetString("name"),
                                Allegiance = reader.GetIdentifier<Allegiance>("allegiance"),
                                Government = reader.GetIdentifier<Government>("government"),
                                IsPlayerFaction = reader.Maybe("is_player_faction", reader.GetBoolean),
                                LastUpdateTime = reader.GetUTCDateTimeOffset("f_updated_at"),
                            };
                            MinorFaction.SystemStatus status = new MinorFaction.SystemStatus
                            {
                                Faction = f,
                                System = s,
                                LastUpdateTime = reader.GetUTCDateTimeOffset("sf_updated_at"),
                            };
                            statuses.Add(f.Name, status);
                        }
                    }
                }
                using (var sfst_cmd = dbc.CreateCommand())
                {
                    sfst_cmd.CommandText = @"
                        SELECT faction, state, current_stage, first_seen_at, current_stage_at
                        FROM system_faction_states
                        WHERE system_id = @id64
                    ";
                    m_db.DeriveParameters(sfst_cmd);
                    sfst_cmd.Parameters["@id64"].Value = (long)s.ID;
                    using (var reader = sfst_cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            string fid = reader.GetString("faction");
                            MinorFaction.StateInfo si = new MinorFaction.StateInfo
                            {
                                State = reader.GetIdentifier<FactionState>("state"),
                                FirstSeenAt = reader.GetUTCDateTimeOffset("first_seen_at"),
                                CurrentStageSeenAt = reader.GetUTCDateTimeOffset("current_stage_at"),
                            };
                            int stage = reader.GetInt32("current_stage");
                            switch ((FactionStateStage)stage)
                            {
                                case FactionStateStage.Pending:    statuses[fid].PendingStates.Add(si);    break;
                                case FactionStateStage.Active:     statuses[fid].ActiveStates.Add(si);     break;
                                case FactionStateStage.Recovering: statuses[fid].RecoveringStates.Add(si); break;
                                default: throw new ApplicationException($"read invalid faction state stage value {stage}");
                            }
                        }
                    }
                }
            }
            return statuses.Values.ToList();
        }

#region SprinklerComponent
        public override void Start()
        {
            base.Start();
            Ticks.Start();
        }

        public override void Stop()
        {
            base.Stop();
            Ticks.Stop();
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);
            if (disposing)
                Ticks?.Dispose();
        }
#endregion

        internal void HandleSystemUpdate(DbConnection dbc, JournalMessages.SystemInfo s, DateTimeOffset timestamp)
        {
            using (var f_cmd = dbc.CreateCommand())
            using (var sf_cmd = dbc.CreateCommand())
            using (var sfst_cmd = dbc.CreateCommand())
            using (var sfst_clear_cmd = dbc.CreateCommand())
            {
                f_cmd.CommandText = @"
                    INSERT INTO factions VALUES (@name, @updated_at, @allegiance, @govt, @pmf)
                    ON CONFLICT (name) DO UPDATE SET
                        updated_at = @updated_at, allegiance = @allegiance, government = @govt";
                m_db.DeriveParameters(f_cmd);
                f_cmd.Prepare();

                sf_cmd.CommandText = @"
                    INSERT INTO system_factions VALUES (@system, @faction, @updated_at, @influence, @happiness)
                    ON CONFLICT (system_id, faction) DO UPDATE SET
                        updated_at = @updated_at, influence = @influence, happiness = @happiness";
                m_db.DeriveParameters(sf_cmd);
                sf_cmd.Prepare();

                sfst_cmd.CommandText = @"
                    INSERT INTO system_faction_states VALUES (@system, @faction, @state, @stage, @time, @time)
                    ON CONFLICT (system_id, faction, state) DO UPDATE
                        SET current_stage = @stage, current_stage_at = @time
                        WHERE excluded.current_stage <> @stage";
                m_db.DeriveParameters(sfst_cmd);
                sfst_cmd.Prepare();

                sfst_clear_cmd.CommandText = @"
                    DELETE FROM system_faction_states
                    WHERE system_id = @system AND faction = @faction AND state NOT IN (SELECT UNNEST(@states::int[]))";
                m_db.DeriveParameters(sfst_clear_cmd);
                sfst_clear_cmd.Prepare();

                foreach (var f in s.Factions)
                {
                    f_cmd.Parameters["@name"].Value = f.Name;
                    f_cmd.Parameters["@updated_at"].Value = timestamp.UtcDateTime;
                    f_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromSymbolicName<Allegiance>(f.Allegiance);
                    f_cmd.Parameters["@govt"].Value = (int)Identifier.FromSymbolicName<Government>(f.Government);
                    f_cmd.Parameters["@pmf"].Value = DBNull.Value;
                    f_cmd.ExecuteNonQuery();

                    sf_cmd.Parameters["@system"].Value = (long)s.ID;
                    sf_cmd.Parameters["@faction"].Value = f.Name;
                    sf_cmd.Parameters["@updated_at"].Value = timestamp.UtcDateTime;
                    sf_cmd.Parameters["@influence"].Value = (float)f.Influence;
                    sf_cmd.Parameters["@happiness"].Value = (int)Identifier.FromSymbolicName<Happiness>(f.Happiness);
                    sf_cmd.ExecuteNonQuery();

                    foreach (var sfst in f.PendingStates)
                    {
                        sfst_cmd.Parameters["@system"].Value = (long)s.ID;
                        sfst_cmd.Parameters["@faction"].Value = f.Name;
                        sfst_cmd.Parameters["@state"].Value = (int)Identifier.FromSymbolicName<FactionState>(sfst.State);
                        sfst_cmd.Parameters["@stage"].Value = (int)FactionStateStage.Pending;
                        sfst_cmd.Parameters["@time"].Value = timestamp.UtcDateTime;
                        sfst_cmd.ExecuteNonQuery();
                    }
                    foreach (var sfst in f.ActiveStates)
                    {
                        sfst_cmd.Parameters["@system"].Value = (long)s.ID;
                        sfst_cmd.Parameters["@faction"].Value = f.Name;
                        sfst_cmd.Parameters["@state"].Value = (int)Identifier.FromSymbolicName<FactionState>(sfst.State);
                        sfst_cmd.Parameters["@stage"].Value = (int)FactionStateStage.Active;
                        sfst_cmd.Parameters["@time"].Value = timestamp.UtcDateTime;
                        sfst_cmd.ExecuteNonQuery();
                    }
                    foreach (var sfst in f.RecoveringStates)
                    {
                        sfst_cmd.Parameters["@system"].Value = (long)s.ID;
                        sfst_cmd.Parameters["@faction"].Value = f.Name;
                        sfst_cmd.Parameters["@state"].Value = (int)Identifier.FromSymbolicName<FactionState>(sfst.State);
                        sfst_cmd.Parameters["@stage"].Value = (int)FactionStateStage.Recovering;
                        sfst_cmd.Parameters["@time"].Value = timestamp.UtcDateTime;
                        sfst_cmd.ExecuteNonQuery();
                    }

                    sfst_clear_cmd.Parameters["@system"].Value = (long)s.ID;
                    sfst_clear_cmd.Parameters["@faction"].Value = f.Name;
                    sfst_clear_cmd.Parameters["@states"].Value
                        = f.PendingStates.Concat(f.ActiveStates).Concat(f.RecoveringStates)
                            .Select(t => (int)Identifier.FromSymbolicName<FactionState>(t.State)).ToArray();
                    sfst_clear_cmd.ExecuteNonQuery();
                }
            }
        }

        private void GenerateDBTables(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateFactions);
            dbc.ExecuteCommand(DBTableSchemas.CreateSystemFactions);
            dbc.ExecuteCommand(DBTableSchemas.CreateSystemFactionStates);

            SetComponentInitialised(dbc, dbm, DateTime.UtcNow, null);
        }

        private void GenerateDBIndexes(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexSystemFactionsUpdatedAt);
        }

        private DBManager m_db;
    }
}
