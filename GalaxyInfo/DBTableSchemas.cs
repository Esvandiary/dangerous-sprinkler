

namespace DangerousSprinkler
{
    public partial class StaticDataManager
    {
        private static class DBTableSchemas
        {
            public static readonly string CreateSystemsStatic = @"
                CREATE TABLE systems_static (
                    id BIGINT PRIMARY KEY,
                    name VARCHAR(64) NOT NULL,
                    pos_x INTEGER NOT NULL,
                    pos_y INTEGER NOT NULL,
                    pos_z INTEGER NOT NULL
                )
            ";

            public static readonly string CreateIndexSystemsStaticName
                = "CREATE INDEX idx_systems_static_name ON systems_static ((lower(name)))";

            public static readonly string CreateStationsStatic = @"
                CREATE TABLE stations_static (
                    market_id BIGINT PRIMARY KEY,
                    name VARCHAR(128) NOT NULL,
                    system_id BIGINT NOT NULL,
                    updated_at TIMESTAMP NOT NULL,
                    max_pad_size VARCHAR(1) NOT NULL,
                    is_planetary BOOLEAN NOT NULL,
                    type VARCHAR(32),
                    sc_distance INTEGER
                )
            ";

            public static readonly string CreateIndexStationsStaticName
                = "CREATE INDEX idx_stations_static_name ON stations_static ((lower(name)))";
            public static readonly string CreateIndexStationsStaticSystemID
                = "CREATE INDEX idx_stations_static_system_id ON stations_static (system_id)";
        }
    }

    public partial class GalaxyInfo
    {
        private static class DBTableSchemas
        {
            public static readonly string CreateSystems = @"
                CREATE TABLE systems (
                    id BIGINT PRIMARY KEY,
                    updated_at TIMESTAMP NOT NULL,
                    controlling_faction VARCHAR(128),
                    allegiance INTEGER,
                    government INTEGER,
                    economy1 INTEGER,
                    economy2 INTEGER,
                    security INTEGER,
                    population BIGINT
                )
            ";

            public static readonly string CreateStations = @"
                CREATE TABLE stations (
                    market_id BIGINT PRIMARY KEY,
                    info_updated_at TIMESTAMP NOT NULL,
                    market_updated_at TIMESTAMP,
                    controlling_faction VARCHAR(128),
                    allegiance INTEGER,
                    government INTEGER,
                    economy1 INTEGER,
                    economy1_p REAL,
                    economy2 INTEGER,
                    economy2_p REAL,
                    has_market BOOLEAN,
                    has_shipyard BOOLEAN,
                    has_outfitting BOOLEAN,
                    has_blackmarket BOOLEAN
                )
            ";

            public static readonly string CreateIndexStationsInfoUpdatedAt
                = "CREATE INDEX idx_stations_info_updated_at ON stations (info_updated_at)";
            public static readonly string CreateIndexStationsMarketUpdatedAt
                = "CREATE INDEX idx_stations_market_updated_at ON stations (market_updated_at)";

            public static readonly string CreateFleetCarriers = @"
                CREATE TABLE fleetcarriers (
                    market_id BIGINT PRIMARY KEY,
                    carrier_id VARCHAR(8) NOT NULL,
                    system_id BIGINT NOT NULL,
                    name VARCHAR(64),
                    info_updated_at TIMESTAMP NOT NULL,
                    market_updated_at TIMESTAMP,
                    sc_distance INTEGER,
                    has_market BOOLEAN,
                    has_shipyard BOOLEAN,
                    has_outfitting BOOLEAN,
                    has_blackmarket BOOLEAN
                )
            ";

            public static readonly string CreateIndexFleetCarriersSystemID
                = "CREATE INDEX idx_fleetcarriers_system_id ON fleetcarriers (system_id)";
            public static readonly string CreateIndexFleetCarriersInfoUpdatedAt
                = "CREATE INDEX idx_fleetcarriers_info_updated_at ON fleetcarriers (info_updated_at)";
            public static readonly string CreateIndexFleetCarriersMarketUpdatedAt
                = "CREATE INDEX idx_fleetcarriers_market_updated_at ON fleetcarriers (market_updated_at)";
        }
    }

    public partial class BGSTracker
    {
        private static class DBTableSchemas
        {
            public static readonly string CreateFactions = @"
                CREATE TABLE factions (
                    name VARCHAR(128) PRIMARY KEY,
                    updated_at TIMESTAMP NOT NULL,
                    allegiance INTEGER NOT NULL,
                    government INTEGER NOT NULL,
                    is_player_faction BOOLEAN
                )
            ";

            public static readonly string CreateSystemFactions = @"
                CREATE TABLE system_factions (
                    system_id BIGINT NOT NULL,
                    faction VARCHAR(128) NOT NULL,
                    updated_at TIMESTAMP NOT NULL,
                    influence REAL NOT NULL,
                    happiness INTEGER,
                    PRIMARY KEY (system_id, faction)
                )
            ";

            public static readonly string CreateIndexSystemFactionsUpdatedAt
                = "CREATE INDEX idx_system_factions_updated_at ON system_factions (updated_at)";

            public static readonly string CreateSystemFactionStates = @"
                CREATE TABLE system_faction_states (
                    system_id BIGINT NOT NULL,
                    faction VARCHAR(128) NOT NULL,
                    state INTEGER NOT NULL,
                    current_stage INTEGER NOT NULL,
                    first_seen_at TIMESTAMP NOT NULL,
                    current_stage_at TIMESTAMP NOT NULL,
                    PRIMARY KEY (system_id, faction, state)
                )
            ";
        }
    }
}