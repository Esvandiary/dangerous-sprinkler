using System.Collections.Generic;
using System.IO;
using System.Text;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace DangerousSprinkler.DataSources
{
    internal static class CSVUtil
    {
        public static IEnumerable<T> GetCSVListingsFunc<T, TMapping>(Stream s) where TMapping : ICsvMapping<T>, new()
        {
            var opts = new CsvParserOptions(true, ',');
            CsvParser<T> parser = new CsvParser<T>(opts, new TMapping());
            foreach (var entry in parser.ReadFromStream(s, Encoding.UTF8))
            {
                yield return entry.Result;
            }
        }
    }
}