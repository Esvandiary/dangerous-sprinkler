using System;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;

namespace DangerousSprinkler.DataSources
{
    internal class DownloadUtil
    {
        public static T GetURL<T>(string url, Func<Stream, T> func)
        {
            using (HttpClient client = new HttpClient())
            {
                var resp = client.Send(new HttpRequestMessage(HttpMethod.Get, url));
                resp.EnsureSuccessStatusCode();
                return func(resp.Content.ReadAsStream());
            }
        }

        private static IEnumerable<string> GetLinesFunc(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
            {
                string line = null;
                while ((line = sr.ReadLine()) != null)
                    yield return line;
            }
        }

        public static IEnumerable<string> GetLines(string url)
        {
            return GetURL(url, GetLinesFunc);
        }

        private static IEnumerable<string> GetGzippedLinesFunc(Stream stream)
        {
            return GetLinesFunc(new GZipStream(stream, CompressionMode.Decompress));
        }

        public static IEnumerable<string> GetGzippedLines(string url)
        {
            return GetURL(url, GetGzippedLinesFunc);
        }

        private static string GetTextFunc(Stream stream)
        {
            using (StreamReader sr = new StreamReader(stream, Encoding.UTF8))
                return sr.ReadToEnd();
        }

        public static string GetText(string url)
        {
            return GetURL(url, GetTextFunc);
        }
    }
}