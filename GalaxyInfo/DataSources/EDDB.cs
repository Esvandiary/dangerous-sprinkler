using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace DangerousSprinkler.DataSources
{
    internal static class EDDB
    {
        // public const string HostAddress = "https://eddb.io";
        public const string HostAddress = "http://localhost:8081";
        public const string PopulatedSystemsURL = HostAddress + "/archive/v6/systems_populated.jsonl";
        public const string StationsURL = HostAddress + "/archive/v6/stations.jsonl";
        public const string FactionsURL = HostAddress + "/archive/v6/factions.jsonl";
        public const string CommoditiesURL = HostAddress + "/archive/v6/commodities.json";
        public const string MarketListingsURL = HostAddress + "/archive/v6/listings.csv";

        public static IEnumerable<SystemEntry> GetPopulatedSystems()
        {
            foreach (var line in DownloadUtil.GetLines(PopulatedSystemsURL))
            {
                if (line.Trim().Length <= 2) continue;
                yield return JsonConvert.DeserializeObject<SystemEntry>(line);
            }
        }

        public static IEnumerable<StationEntry> GetStations()
        {
            foreach (var line in DownloadUtil.GetLines(StationsURL))
            {
                if (line.Trim().Length <= 2) continue;
                yield return JsonConvert.DeserializeObject<StationEntry>(line);
            }
        }

        public static IEnumerable<FactionEntry> GetFactions()
        {
            foreach (var line in DownloadUtil.GetLines(FactionsURL))
            {
                if (line.Trim().Length <= 2) continue;
                yield return JsonConvert.DeserializeObject<FactionEntry>(line);
            }
        }

        public static List<CommodityEntry> GetCommodities()
        {
            return JsonConvert.DeserializeObject<List<CommodityEntry>>(DownloadUtil.GetText(CommoditiesURL));
        }

        public static IEnumerable<MarketListingEntry> GetMarketListings()
        {
            return DownloadUtil.GetURL(MarketListingsURL, CSVUtil.GetCSVListingsFunc<MarketListingEntry, CsvMarketListingMapping>);
        }


        public class SystemFactionEntry
        {
            [JsonProperty("minor_faction_id")]
            public ulong ID { get; set; }
            [JsonProperty("active_states")]
            public StateEntry[] ActiveStates { get; set; }
            [JsonProperty("pending_states")]
            public StateEntry[] PendingStates { get; set; }
            [JsonProperty("recovering_states")]
            public StateEntry[] RecoveringStates { get; set; }
        }

        public class StateEntry
        {
            [JsonProperty("id")]
            public uint ID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public class SystemEntry
        {
            [JsonProperty("ed_system_address")]
            public ulong? SystemAddress { get; set; }
            [JsonProperty("id")]
            public ulong EDDBID { get; set; }
            [JsonProperty("edsm_id")]
            public ulong? EDSMID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("updated_at")]
            public long UpdatedAtUnixTime { get; set; }
            public DateTimeOffset UpdatedAt { get { return DateTimeOffset.FromUnixTimeSeconds(UpdatedAtUnixTime); } }
            [JsonProperty("x")]
            public float PositionX { get; set; }
            [JsonProperty("y")]
            public float PositionY { get; set; }
            [JsonProperty("z")]
            public float PositionZ { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("states")]
            public StateEntry[] States { get; set; }
            [JsonProperty("security")]
            public string SecurityLevel { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("primary_economy")]
            public string PrimaryEconomy { get; set; }
            [JsonProperty("secondary_economy")]
            public string SecondaryEconomy { get; set; }
            [JsonProperty("reserve_type")]
            public string ReserveLevel { get; set; }
            [JsonProperty("controlling_minor_faction_id")]
            public ulong? ControllingFactionID { get; set; }
            [JsonProperty("controlling_minor_faction")]
            public string ControllingFactionName { get; set; }
            [JsonProperty("minor_faction_presences")]
            public SystemFactionEntry[] Factions { get; set; }
        }

        public class StationEntry
        {
            [JsonProperty("ed_market_id")]
            public ulong MarketID { get; set; }
            [JsonProperty("id")]
            public ulong EDDBID { get; set; }
            [JsonProperty("system_id")]
            public ulong SystemEDDBID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("body_id")]
            public ulong EDDBBodyID { get; set; }
            [JsonProperty("updated_at")]
            public long UpdatedAtUnixTime { get; set; }
            public DateTimeOffset UpdatedAt { get { return DateTimeOffset.FromUnixTimeSeconds(UpdatedAtUnixTime); } }
            [JsonProperty("max_landing_pad_size")]
            public string MaxPadSize { get; set; }
            [JsonProperty("distance_to_star")]
            public ulong DistanceFromStar { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("states")]
            public StateEntry[] States { get; set; }
            [JsonProperty("type")]
            public string StationType { get; set; }
            [JsonProperty("has_market")]
            public bool HasMarket { get; set; }
            [JsonProperty("has_blackmarket")]
            public bool HasBlackMarket { get; set; }
            [JsonProperty("prohibited_commodities")]
            public string[] ProhibitedCommodities { get; set; }
            [JsonProperty("economies")]
            public string[] Economies { get; set; }
            [JsonProperty("market_updated_at")]
            public long? MarketUpdatedAtUnixTime { get; set; }
            public DateTimeOffset? MarketUpdatedAt { get { return DateTimeUtil.FromUnixTimeSeconds(MarketUpdatedAtUnixTime); } }
            [JsonProperty("is_planetary")]
            public bool IsPlanetary { get; set; }
            [JsonProperty("controlling_minor_faction_id")]
            public ulong? ControllingFactionID { get; set; }
        }

        public class FactionEntry
        {
            [JsonProperty("id")]
            public ulong ID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("updated_at")]
            public long UpdatedAtUnixTime { get; set; }
            public DateTimeOffset UpdatedAt { get { return DateTimeOffset.FromUnixTimeSeconds(UpdatedAtUnixTime); } }
            [JsonProperty("is_player_faction")]
            public bool IsPlayerFaction { get; set; }
            [JsonProperty("home_system_id")]
            public ulong? HomeSystemEDDBID { get; set; }
        }

        public class CategoryEntry
        {
            [JsonProperty("id")]
            public ulong ID { get; set; }
            [JsonProperty("name")]
            public string name { get; set; }
            
        }

        public class CommodityEntry
        {
            [JsonProperty("ed_id")]
            public ulong ID { get; set; }
            [JsonProperty("id")]
            public ulong EDDBID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("category")]
            public CategoryEntry Category { get; set; }
            [JsonProperty("average_price")]
            public long? AveragePrice { get; set; }
            [JsonProperty("max_buy_price")]
            public long? MaxBuyPrice { get; set; }
            [JsonProperty("max_sell_price")]
            public long? MaxSellPrice { get; set; }
            [JsonProperty("min_buy_price")]
            public long? MinBuyPrice { get; set; }
            [JsonProperty("min_sell_price")]
            public long? MinSellPrice { get; set; }
            [JsonProperty("is_non_marketable")]
            public bool IsNonMarketable { get; set; }
            [JsonProperty("is_rare")]
            public bool IsRare { get; set; }
        }

        public class MarketListingEntry
        {
            public ulong ID { get; set; }
            public ulong EDDBStationID { get; set; }
            public ulong EDDBCommodityID { get; set; }
            public long Supply { get; set; }
            public int SupplyBracketNum { get; set; }
            public CommodityBracket SupplyBracket { get { return (CommodityBracket)SupplyBracketNum; } }
            public int BuyPrice { get; set; }
            public int SellPrice { get; set; }
            public long Demand { get; set; }
            public int DemandBracketNum { get; set; }
            public CommodityBracket DemandBracket { get { return (CommodityBracket)DemandBracketNum; } }
            public long LastUpdatedAtUnixTime { get; set; }
            public DateTimeOffset LastUpdatedAt { get { return DateTimeOffset.FromUnixTimeSeconds(LastUpdatedAtUnixTime); } }
        }

        private class CsvMarketListingMapping : CsvMapping<MarketListingEntry>
        {
            public CsvMarketListingMapping() : base()
            {
                MapProperty(0, x => x.ID);
                MapProperty(1, x => x.EDDBStationID);
                MapProperty(2, x => x.EDDBCommodityID);
                MapProperty(3, x => x.Supply);
                MapProperty(4, x => x.SupplyBracket);
                MapProperty(5, x => x.BuyPrice);
                MapProperty(6, x => x.SellPrice);
                MapProperty(7, x => x.Demand);
                MapProperty(8, x => x.DemandBracket);
                MapProperty(9, x => x.LastUpdatedAtUnixTime);
            }
        }
    }
}