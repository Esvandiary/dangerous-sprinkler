using System;
using System.Collections.Generic;
using System.Globalization;
using Newtonsoft.Json;

namespace DangerousSprinkler.DataSources
{
    internal static class EDSM
    {
        // public const string HostAddress = "https://edsm.net";
        public const string HostAddress = "http://localhost:8081";
        public const string PopulatedSystemsURL = HostAddress + "/dump/systemsPopulated.json.gz";

        public static IEnumerable<SystemEntry> GetPopulatedSystems()
        {
            foreach (var line in DownloadUtil.GetGzippedLines(PopulatedSystemsURL))
            {
                if (line.Trim().Length <= 2) continue;
                yield return JsonConvert.DeserializeObject<SystemEntry>(line.TrimEnd(','));
            }
        }

        public class SystemEntry
        {
            [JsonProperty("id64")]
            public ulong ID64 { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("coords")]
            public Coords Location { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("state")]
            public string State { get; set; }
            [JsonProperty("economy")]
            public string Economy { get; set; }
            [JsonProperty("security")]
            public string Security { get; set; }
            [JsonProperty("population")]
            public long? Population { get; set; }
            [JsonProperty("controllingFaction")]
            public ControllingFactionEntry ControllingFaction { get; set; }
            [JsonProperty("stations")]
            public StationEntry[] Stations { get; set; }
            [JsonProperty("factions")]
            public SystemFactionEntry[] Factions { get; set; }
        }

        public class StationEntry
        {
            [JsonProperty("marketId")]
            public ulong MarketID { get; set; }
            [JsonProperty("type")]
            public string Type { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("distanceToArrival")]
            public double? DistanceFromStar { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("economy")]
            public string PrimaryEconomy { get; set; }
            [JsonProperty("secondEconomy")]
            public string SecondaryEconomy { get; set; }
            [JsonProperty("haveMarket")]
            public bool HasMarket { get; set; }
            [JsonProperty("haveShipyard")]
            public bool HasShipyard { get; set; }
            [JsonProperty("haveOutfitting")]
            public bool HasOutfitting { get; set; }
            [JsonProperty("otherServices")]
            public string[] OtherServices { get; set; }
            [JsonProperty("controllingFaction")]
            public ControllingFactionEntry ControllingFaction { get; set; }
            [JsonProperty("updateTime")]
            public UpdateTimes UpdatedAt { get; set; }
        }

        public class SystemFactionEntry
        {
            [JsonProperty("id")]
            public ulong ID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
            [JsonProperty("allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("government")]
            public string Government { get; set; }
            [JsonProperty("influence")]
            public double Influence { get; set; }
            [JsonProperty("activeStates")]
            public FactionStateEntry[] ActiveStates { get; set; }
            [JsonProperty("recoveringStates")]
            public FactionStateEntry[] RecoveringStates { get; set; }
            [JsonProperty("pendingStates")]
            public FactionStateEntry[] PendingStates { get; set; }
            [JsonProperty("happiness")]
            public string Happiness { get; set; }
            [JsonProperty("isPlayer")]
            public bool IsPlayerFaction { get; set; }
            [JsonProperty("lastUpdate")]
            public long UpdatedAtUnixTime { get; set; }
            [JsonIgnore]
            public DateTimeOffset UpdatedAt { get => DateTimeOffset.FromUnixTimeSeconds(UpdatedAtUnixTime); }
        }

        public class FactionStateEntry
        {
            [JsonProperty("state")]
            public string State { get; set; }
            [JsonProperty("trend")]
            public double? Trend { get; set; }
        }

        public class ControllingFactionEntry
        {
            [JsonProperty("id")]
            public ulong? ID { get; set; }
            [JsonProperty("name")]
            public string Name { get; set; }
        }

        public class Coords
        {
            [JsonProperty("x")]
            public double X { get; set; }
            [JsonProperty("y")]
            public double Y { get; set; }
            [JsonProperty("z")]
            public double Z { get; set; }
        }

        public class UpdateTimes
        {
            [JsonProperty("information")]
            public string InformationText { get; set; }
            [JsonIgnore]
            public DateTimeOffset Information { get => DateTimeOffset.Parse(InformationText, null, DateTimeStyles.AssumeUniversal); }
            [JsonProperty("market")]
            public string MarketText { get; set; }
            [JsonIgnore]
            public DateTimeOffset Market { get => DateTimeOffset.Parse(MarketText, null, DateTimeStyles.AssumeUniversal); }
            [JsonProperty("shipyard")]
            public string ShipyardText { get; set; }
            [JsonIgnore]
            public DateTimeOffset Shipyard { get => DateTimeOffset.Parse(ShipyardText, null, DateTimeStyles.AssumeUniversal); }
            [JsonProperty("outfitting")]
            public string OutfittingText { get; set; }
            [JsonIgnore]
            public DateTimeOffset Outfitting { get => DateTimeOffset.Parse(OutfittingText, null, DateTimeStyles.AssumeUniversal); }
        }
    }
}