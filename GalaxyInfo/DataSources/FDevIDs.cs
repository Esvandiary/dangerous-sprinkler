using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using TinyCsvParser;
using TinyCsvParser.Mapping;

namespace DangerousSprinkler.DataSources
{
    internal static class FDevIDs
    {
        // public const string HostAddress = "https://raw.githubusercontent.com";
        public const string HostAddress = "http://localhost:8081";
        public const string CommoditiesURL = HostAddress + "/EDCD/FDevIDs/master/commodity.csv";
        public const string RaresURL = HostAddress + "/EDCD/FDevIDs/master/rare_commodity.csv";

        public static IEnumerable<CommodityEntry> GetCommodities()
            => DownloadUtil.GetURL(CommoditiesURL, CSVUtil.GetCSVListingsFunc<CommodityEntry, CsvCommodityEntryMapping>);
        public static IEnumerable<CommodityEntry> GetRareCommodities()
            => DownloadUtil.GetURL(RaresURL, CSVUtil.GetCSVListingsFunc<CommodityEntry, CsvRareCommodityEntryMapping>);

        private class CsvCommodityEntryMapping : CsvMapping<CommodityEntry>
        {
            public CsvCommodityEntryMapping() : base()
            {
                MapProperty(0, x => x.ID);
                MapProperty(1, x => x.SymbolicName);
                MapProperty(2, x => x.Category);
                MapProperty(3, x => x.LocalisedName);
            }
        }

        private class CsvRareCommodityEntryMapping : CsvMapping<CommodityEntry>
        {
            public CsvRareCommodityEntryMapping() : base()
            {
                MapProperty(0, x => x.ID);
                MapProperty(1, x => x.SymbolicName);
                MapProperty(2, x => x.MarketID);
                MapProperty(3, x => x.Category);
                MapProperty(4, x => x.LocalisedName);
            }
        }
    }
}