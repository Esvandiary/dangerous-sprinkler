using System;
using System.Data.Common;
using System.Linq;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using DangerousSprinkler.DataSources;
using DangerousSprinkler.Extensions;
using Newtonsoft.Json.Linq;

namespace DangerousSprinkler
{
    public partial class GalaxyInfo : SprinklerComponent, IReceiverListener
    {
        public GalaxyInfo(CoreServices cs, DBManager db, StaticDataManager sdm, BGSTracker bgs)
            : base(cs, "GalaxyInfo", "Galaxy Data")
        {
            m_db = db;
            m_sdm = sdm;
            BGS = bgs;

            m_processingThread = new Thread(RunProcessingThread);

            m_db.RegisterGenerateStep(GenerateDBTables);
            m_db.RegisterPostGenerateStep(GenerateDBIndexes);
        }

#region IReceiverListener
        public ReadOnlyCollection<string> SchemaRefs { get; } = new(new[]{ SchemaURLs.Journal_v10 });
        public BlockingCollection<JObject> Queue { get; } = new();
#endregion

#region SprinklerComponent
        public override void Start()
        {
            base.Start();
            m_processingThread.Start();
        }

        public override void Stop()
        {
            base.Stop();
            if (m_processingThread.IsAlive)
                m_processingThread.Join();
        }
#endregion

        public CommodityEntry GetCommodity(ulong id) => m_sdm.GetCommodity(id);
        public CommodityEntry GetCommodity(string symbol) => m_sdm.GetCommodity(symbol);

        public BGSTracker BGS { get; private set; }

        private void RunProcessingThread()
        {
            while (IsRunning)
            {
                try
                {
                    JObject o = null;
                    while (IsRunning && !Queue.TryTake(out o, 250));
                    if (!IsRunning) break;
                    if (o.Value<string>("$schemaRef") == SchemaURLs.Journal_v10)
                    {
                        if (o.ContainsKey("message") && o.Value<JObject>("message").ContainsKey("event"))
                        {
                            JObject msg = o.Value<JObject>("message");
                            string evt = msg.Value<string>("event");
                            switch (msg.Value<string>("event"))
                            {
                                case "FSDJump":
                                case "Location":
                                case "Docked":
                                    HandleSystemStationUpdate(msg);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"EXCEPTION processing journal message: {ex.Message}");
                    Logger.Error($"Stack trace: {ex.StackTrace}");
                }
            }
        }

        private void HandleSystemStationUpdate(JObject msg)
        {
            string evt = msg.Value<string>("event");
            DateTimeOffset timestamp = DateTimeUtil.FromISO8601(msg.Value<string>("timestamp"));
            bool hasSystemInfo = (evt == "FSDJump" || evt == "Location");
            bool hasStationInfo = (evt == "Docked" || (evt == "Location" && msg.Value<bool>("Docked")));

            using (var dbc = m_db.CreateConnection())
            using (var tx = dbc.BeginTransaction())
            {
                if (hasSystemInfo)
                {
                    JournalMessages.SystemInfo s = msg.ToObject<JournalMessages.SystemInfo>();
                    Logger.Debug($"Going to update system info for {s.Name} ({s.ID})");
                    BGS.HandleSystemUpdate(dbc, s, timestamp);

                    using (var sys_cmd = dbc.CreateCommand())
                    {
                        sys_cmd.CommandText = @"
                            UPDATE systems SET
                                updated_at = @updated_at, controlling_faction = @faction,
                                allegiance = @allegiance, government = @government,
                                economy1 = @economy1, economy2 = @economy2,
                                security = @security, population = @population
                            WHERE id = @id64";
                        m_db.DeriveParameters(sys_cmd);
                        sys_cmd.Parameters["@id64"].Value = (long)s.ID;
                        sys_cmd.Parameters["@updated_at"].Value = timestamp.UtcDateTime;
                        sys_cmd.Parameters["@faction"].SetValueOrNull(s.OwnerFaction?.Name);
                        sys_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromSymbolicName<Allegiance>(s.Allegiance);
                        sys_cmd.Parameters["@government"].Value = (int)Identifier.FromSymbolicName<Government>(s.Government);
                        sys_cmd.Parameters["@economy1"].Value = (int)Identifier.FromSymbolicName<EconomyType>(s.PrimaryEconomy);
                        sys_cmd.Parameters["@economy2"].Value = (int)Identifier.FromSymbolicName<EconomyType>(s.SecondaryEconomy);
                        sys_cmd.Parameters["@security"].Value = (int)Identifier.FromSymbolicName<SecurityLevel>(s.Security);
                        sys_cmd.Parameters["@population"].Value = s.Population;
                        sys_cmd.ExecuteNonQuery();
                    }
                }
                if (hasStationInfo)
                {
                    JournalMessages.StationInfo st = msg.ToObject<JournalMessages.StationInfo>();
                    StationInfo stype = StationInfo.FromName(st.Type);
                    if (stype == null)
                        Logger.Warning("Unknown station type \"{Type}\"", st.Type);

                    if (stype != null && stype.IsFleetCarrier)
                    {
                        Logger.Debug($"Going to update info for fleet carrier {st.Name} ({st.MarketID}) in {st.SystemName}");
                        using (var fc_cmd = dbc.CreateCommand())
                        {
                            fc_cmd.CommandText = @"
                                INSERT INTO fleetcarriers VALUES (
                                    @market_id, @carrier_id, @system, NULL, @time, NULL,
                                    @sc_distance, @market, @shipyard, @outfitting, @blackmarket)
                                ON CONFLICT (market_id) DO UPDATE SET
                                    carrier_id = @carrier_id, system_id = @system, info_updated_at = @time,
                                    sc_distance = @sc_distance, has_market = @market, has_shipyard = @shipyard,
                                    has_outfitting = @outfitting, has_blackmarket = @blackmarket";
                            m_db.DeriveParameters(fc_cmd);
                            fc_cmd.Parameters["@market_id"].Value = (long)st.MarketID;
                            fc_cmd.Parameters["@carrier_id"].Value = st.Name;
                            fc_cmd.Parameters["@system"].Value = (long)st.SystemID;
                            fc_cmd.Parameters["@time"].Value = timestamp.UtcDateTime;
                            fc_cmd.Parameters["@sc_distance"].Value = (int)st.DistanceFromStar;
                            fc_cmd.Parameters["@market"].Value = st.Services.Contains("commodities");
                            fc_cmd.Parameters["@shipyard"].Value = st.Services.Contains("shipyard");
                            fc_cmd.Parameters["@outfitting"].Value = st.Services.Contains("outfitting");
                            fc_cmd.Parameters["@blackmarket"].Value = st.Services.Contains("blackmarket");
                            fc_cmd.ExecuteNonQuery();
                        }
                    }
                    else
                    {
                        Logger.Debug($"Going to update info for station {st.Name} ({st.MarketID}) in {st.SystemName}");
                        using (var st_cmd = dbc.CreateCommand())
                        {
                            st_cmd.CommandText = @"
                                UPDATE stations SET
                                    info_updated_at = @time, controlling_faction = @faction,
                                    allegiance = @allegiance, government = @government,
                                    economy1 = @econ1, economy1_p = @econ1p, economy2 = @econ2, economy2_p = @econ2p,
                                    has_market = @market, has_shipyard = @shipyard,
                                    has_outfitting = @outfitting, has_blackmarket = @blackmarket
                                WHERE market_id = @market_id";
                            m_db.DeriveParameters(st_cmd);
                            st_cmd.Parameters["@market_id"].Value = (long)st.MarketID;
                            st_cmd.Parameters["@time"].Value = timestamp.UtcDateTime;
                            st_cmd.Parameters["@faction"].SetValueOrNull(st.OwnerFaction?.Name);
                            st_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromSymbolicName<Allegiance>(st.Allegiance);
                            st_cmd.Parameters["@government"].Value = (int)Identifier.FromSymbolicName<Allegiance>(st.Government);
                            if (st.Economies.Length >= 1)
                            {
                                st_cmd.Parameters["@econ1"].Value = (int)Identifier.FromSymbolicName<EconomyType>(st.Economies[0].Name);
                                st_cmd.Parameters["@econ1p"].Value = (float)(st.Economies[0].Proportion);
                            }
                            else
                            {
                                st_cmd.Parameters["@econ1"].Value = (int)EconomyType.None;
                                st_cmd.Parameters["@econ1p"].Value = 0.0f;
                            }
                            if (st.Economies.Length >= 2)
                            {
                                st_cmd.Parameters["@econ2"].Value = (int)Identifier.FromSymbolicName<EconomyType>(st.Economies[1].Name);
                                st_cmd.Parameters["@econ2p"].Value = (float)(st.Economies[1].Proportion);
                            }
                            else
                            {
                                st_cmd.Parameters["@econ2"].Value = (int)EconomyType.None;
                                st_cmd.Parameters["@econ2p"].Value = 0.0f;
                            }
                            st_cmd.Parameters["@market"].Value = st.Services.Contains("commodities");
                            st_cmd.Parameters["@shipyard"].Value = st.Services.Contains("shipyard");
                            st_cmd.Parameters["@outfitting"].Value = st.Services.Contains("outfitting");
                            st_cmd.Parameters["@blackmarket"].Value = st.Services.Contains("blackmarket");
                            st_cmd.ExecuteNonQuery();
                        }
                    }
                }
                tx.Commit();
            }
        }

        private IList<StarSystemInfo> GetSystemInfos(string column, object value)
        {
            using (var dbc = m_db.CreateConnection())
            {
                IList<StarSystemInfo> systems = new List<StarSystemInfo>();
                using (var sys_cmd = dbc.CreateCommand())
                {
                    sys_cmd.CommandText = $"SELECT id, name, pos_x, pos_y, pos_z FROM systems_static WHERE {column} = @v";
                    m_db.DeriveParameters(sys_cmd);
                    sys_cmd.Parameters["@v"].Value = value;
                    using (var reader = sys_cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StarSystemInfo s = new StarSystemInfo
                            {
                                ID = (ulong)reader.GetInt64("id"),
                                Name = reader.GetString("name"),
                                Position = new System.Numerics.Vector3(
                                    (float)Coordinates.X.ToGalaxy(reader.GetInt64("pos_x")),
                                    (float)Coordinates.Y.ToGalaxy(reader.GetInt64("pos_y")),
                                    (float)Coordinates.Z.ToGalaxy(reader.GetInt64("pos_z"))),
                            };
                            systems.Add(s);
                        }
                    }
                }
                return systems;
            }
        }
        public StarSystemInfo GetSystemInfo(ulong id64)
            => GetSystemInfos("id", id64).FirstOrDefault();
        public IList<StarSystemInfo> GetSystemInfos(string name)
            => GetSystemInfos("lower(name)", name.ToLowerInvariant());

        private IList<StarSystem> GetSystems(string column, object value, bool getFactions, bool getStations)
        {
            using (var dbc = m_db.CreateConnection())
            {
                IList<StarSystem> systems = new List<StarSystem>();
                Dictionary<ulong, string> controllingFaction = new Dictionary<ulong, string>();
                using (var sys_cmd = dbc.CreateCommand())
                {
                    sys_cmd.CommandText = $@"
                        SELECT
                            s.id AS id, name, pos_x, pos_y, pos_z,
                            updated_at, controlling_faction, allegiance,
                            government, economy1, economy2, security, population
                        FROM systems_static ss
                        INNER JOIN systems s ON ss.id = s.id
                        WHERE {column} = @v
                    ";
                    m_db.DeriveParameters(sys_cmd);
                    sys_cmd.Parameters["@v"].Value = value;
                    using (var reader = sys_cmd.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            StarSystem s = new StarSystem
                            {
                                ID = (ulong)reader.GetInt64("id"),
                                Name = reader.GetString("name"),
                                Position = new System.Numerics.Vector3(
                                    (float)Coordinates.X.ToGalaxy(reader.GetInt64("pos_x")),
                                    (float)Coordinates.Y.ToGalaxy(reader.GetInt64("pos_y")),
                                    (float)Coordinates.Z.ToGalaxy(reader.GetInt64("pos_z"))),
                                LastUpdateTime = reader.GetUTCDateTimeOffset("updated_at"),
                                Allegiance = reader.GetIdentifier<Allegiance>("allegiance"),
                                Government = reader.GetIdentifier<Government>("government"),
                                PrimaryEconomy = reader.GetIdentifier<EconomyType>("economy1"),
                                SecondaryEconomy = reader.GetIdentifier<EconomyType>("economy2"),
                                SecurityLevel = reader.GetIdentifier<SecurityLevel>("security"),
                                Population = reader.Maybe("population", reader.GetInt64, 0),
                            };
                            systems.Add(s);

                            controllingFaction[s.ID] = reader.Maybe("controlling_faction", reader.GetString, null);
                        }
                    }
                }

                if (getFactions)
                {
                    foreach (var s in systems)
                    {
                        s.Factions = BGS.GetFactions(s);
                        s.ControllingFaction = s.Factions.FirstOrDefault(sf => sf.Faction.Name == controllingFaction[s.ID]);
                    }
                }
                if (getStations)
                {
                    foreach (var s in systems)
                    {
                        s.Markets = new List<IMarket>();
                        using (DbCommand st_cmd = dbc.CreateCommand())
                        {
                            st_cmd.CommandText = @"
                                SELECT
                                    ss.market_id AS market_id, name, system_id,
                                    s.info_updated_at AS updated_at,
                                    max_pad_size, is_planetary, type, sc_distance,
                                    market_updated_at, controlling_faction,
                                    allegiance, government, economy1, economy1_p, economy2, economy2_p,
                                    has_market, has_shipyard, has_outfitting, has_blackmarket
                                FROM stations_static ss
                                INNER JOIN stations s ON ss.market_id = s.market_id
                                WHERE ss.system_id = @id64
                                ORDER BY sc_distance
                            ";
                            m_db.DeriveParameters(st_cmd);
                            st_cmd.Parameters["@id64"].Value = (long)s.ID;
                            using (var reader = st_cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    string fid = reader.Maybe("controlling_faction", reader.GetString, null);
                                    Station st = new Station
                                    {
                                        MarketID = (ulong)reader.GetInt64("market_id"),
                                        Name = reader.GetString("name"),
                                        System = s,
                                        LastUpdateTime = reader.GetUTCDateTimeOffset("updated_at"),
                                        MaxPadSize = Identifier.FromSymbolicName<PadSize>(reader.GetString("max_pad_size")),
                                        IsPlanetary = reader.GetBoolean("is_planetary"),
                                        Type = StationInfo.FromName(reader.GetString("type"))?.Type ?? StationType.Unknown,
                                        DistanceFromEntryPoint = (uint?)reader.Maybe("sc_distance", reader.GetInt32),
                                        MarketUpdateTime = reader.Maybe("market_updated_at", reader.GetUTCDateTimeOffset),
                                        ControllingFaction = (getFactions) ? s.Factions.FirstOrDefault(sf => sf.Faction.Name == fid) : null,
                                        Allegiance = reader.GetIdentifier<Allegiance>("allegiance"),
                                        Government = reader.GetIdentifier<Government>("government"),
                                        PrimaryEconomy = new Economy {
                                            Type = reader.GetIdentifier<EconomyType>("economy1"),
                                            Proportion = reader.Maybe("economy1_p", reader.GetFloat) },
                                        SecondaryEconomy = new Economy {
                                            Type = reader.GetIdentifier<EconomyType>("economy2"),
                                            Proportion = reader.Maybe("economy2_p", reader.GetFloat) },
                                        HasMarket = reader.GetBoolean("has_market"),
                                        HasShipyard = reader.GetBoolean("has_shipyard"),
                                        HasOutfitting = reader.GetBoolean("has_outfitting"),
                                        HasBlackMarket = reader.GetBoolean("has_blackmarket"),
                                    };
                                    s.Markets.Add(st);
                                }
                            }
                        }
                        using (DbCommand fc_cmd = dbc.CreateCommand())
                        {
                            fc_cmd.CommandText = @"
                                SELECT
                                    market_id, carrier_id, system_id, name,
                                    info_updated_at, market_updated_at, sc_distance,
                                    has_market, has_shipyard, has_outfitting, has_blackmarket
                                FROM fleetcarriers
                                WHERE system_id = @id64
                                ORDER BY sc_distance
                            ";
                            m_db.DeriveParameters(fc_cmd);
                            fc_cmd.Parameters["@id64"].Value = (long)s.ID;
                            using (var reader = fc_cmd.ExecuteReader())
                            {
                                while (reader.Read())
                                {
                                    FleetCarrier fc = new FleetCarrier
                                    {
                                        MarketID = (ulong)reader.GetInt64("market_id"),
                                        Name = reader.GetString("carrier_id"),
                                        CarrierName = reader.Maybe("name", reader.GetString, null),
                                        System = s,
                                        LastUpdateTime = reader.GetUTCDateTimeOffset("info_updated_at"),
                                        DistanceFromEntryPoint = (uint?)reader.Maybe("sc_distance", reader.GetInt32),
                                        MarketUpdateTime = reader.Maybe("market_updated_at", reader.GetUTCDateTimeOffset),
                                        HasMarket = reader.GetBoolean("has_market"),
                                        HasShipyard = reader.GetBoolean("has_shipyard"),
                                        HasOutfitting = reader.GetBoolean("has_outfitting"),
                                        HasBlackMarket = reader.GetBoolean("has_blackmarket"),
                                    };
                                    s.Markets.Add(fc);
                                }
                            }
                        }
                        s.Markets.OrderBy(t => t.DistanceFromEntryPoint);
                    }
                }
                return systems;
            }
        }

        public StarSystem GetSystem(ulong id64, bool populateFactions = true, bool populateStations = true)
            => GetSystems("id", id64, populateFactions, populateStations).FirstOrDefault();
        public IList<StarSystem> GetSystems(string name, bool populateFactions = true, bool populateStations = true)
            => GetSystems("lower(name)", name.ToLowerInvariant(), populateFactions, populateStations);

        private void GenerateDBTables(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateSystems);
            dbc.ExecuteCommand(DBTableSchemas.CreateStations);
            dbc.ExecuteCommand(DBTableSchemas.CreateFleetCarriers);

            SetComponentInitialised(dbc, dbm, DateTime.UtcNow, null);
        }

        private void GenerateDBIndexes(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexStationsInfoUpdatedAt);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexStationsMarketUpdatedAt);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexFleetCarriersSystemID);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexFleetCarriersInfoUpdatedAt);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexFleetCarriersMarketUpdatedAt);
        }

        private DBManager m_db;
        private StaticDataManager m_sdm;
        private Thread m_processingThread;
    }
}