using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Reflection;
using DangerousSprinkler.Extensions;

namespace DangerousSprinkler
{
    public enum Allegiance
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("$faction_none;", "None")]
        None = 1,
        [SymbolicName("$faction_Independent;", "Independent")]
        Independent = 2,
        [SymbolicName("$faction_Federation;", "Federation")]
        Federation = 3,
        [SymbolicName("$faction_Empire;", "Empire")]
        Empire = 4,
        [SymbolicName("$faction_Alliance;", "Alliance")]
        Alliance = 5,
        [SymbolicName("$faction_Pirate;", "Pirate")]
        Pirate = 6,
    }

    public enum Government
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("$government_None;", "None")]
        None = 1,
        [SymbolicName("$government_Anarchy;", "Anarchy")]
        Anarchy = 2,
        [SymbolicName("$government_Communism;", "Communism")]
        Communism = 3,
        [SymbolicName("$government_Confederacy;", "Confederacy")]
        Confederacy = 4,
        [SymbolicName("$government_Cooperative;", "Cooperative")]
        Cooperative = 5,
        [SymbolicName("$government_Corporate;", "Corporate")]
        Corporate = 6,
        [SymbolicName("$government_Democracy;", "Democracy")]
        Democracy = 7,
        [SymbolicName("$government_Dictatorship;", "Dictatorship")]
        Dictatorship = 8,
        [SymbolicName("$government_Feudal;", "Feudal")]
        Feudal = 9,
        [SymbolicName("$government_Imperial;", "Imperial")]
        Imperial = 10,
        [SymbolicName("$government_Patronage;", "Patronage")]
        Patronage = 11,
        [SymbolicName("$government_PrisonColony;", "Prison Colony")]
        PrisonColony = 12,
        [SymbolicName("$government_Theocracy;", "Theocracy")]
        Theocracy = 13,
        [SymbolicName("$government_Engineer;", "Engineer")]
        Engineer = 14,
        [SymbolicName("$government_Carrier;", "Private Ownership")]
        Carrier = 15,
    }

    public enum EconomyType
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("$economy_None;", "None")]
        None = 1,
        [SymbolicName("$economy_Agri;", "Agriculture")]
        Agriculture = 2,
        [SymbolicName("$economy_Colony;", "Colony")]
        Colony = 3,
        [SymbolicName("$economy_Extraction;", "Extraction")]
        Extraction = 4,
        [SymbolicName("$economy_HighTech;", "High Tech")]
        HighTech = 5,
        [SymbolicName("$economy_Industrial;", "Industrial")]
        Industrial = 6,
        [SymbolicName("$economy_Military;", "Military")]
        Military = 7,
        [SymbolicName("$economy_Refinery;", "Refinery")]
        Refinery = 8,
        [SymbolicName("$economy_Service;", "Service")]
        Service = 9,
        [SymbolicName("$economy_Terraforming;", "Terraforming")]
        Terraforming = 10,
        [SymbolicName("$economy_Tourism;", "Tourism")]
        Tourism = 11,
        [SymbolicName("$economy_Prison;", "Prison")]
        Prison = 12,
        [SymbolicName("$economy_Damaged;", "Damaged")]
        Damaged = 13,
        [SymbolicName("$economy_Rescue;", "Rescue")]
        Rescue = 14,
        [SymbolicName("$economy_Repair;", "Repair")]
        Repair = 15,
        [SymbolicName("$economy_Carrier;", "Private Enterprise")]
        PrivateEnterprise = 16,
    }

    public enum FactionState
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("None", "None")]
        None = 1,
        [SymbolicName("Boom", "Boom")]
        Boom = 2,
        [SymbolicName("Bust", "Bust")]
        Bust = 3,
        [SymbolicName("CivilUnrest", "Civil Unrest")]
        CivilUnrest = 4,
        [SymbolicName("CivilWar", "Civil War")]
        CivilWar = 5,
        [SymbolicName("Election", "Election")]
        Election = 6,
        [SymbolicName("Expansion", "Expansion")]
        Expansion = 7,
        [SymbolicName("Famine", "Famine")]
        Famine = 8,
        [SymbolicName("Investment", "Investment")]
        Investment = 9,
        [SymbolicName("Lockdown", "Lockdown")]
        Lockdown = 10,
        [SymbolicName("Outbreak", "Outbreak")]
        Outbreak = 11,
        [SymbolicName("Retreat", "Retreat")]
        Retreat = 12,
        [SymbolicName("War", "War")]
        War = 13,
        [SymbolicName("CivilLiberty", "Civil Liberty")]
        CivilLiberty = 14,
        [SymbolicName("PirateAttack", "Pirate Attack")]
        PirateAttack = 15,
        [SymbolicName("Blight", "Blight")]
        Blight = 16,
        [SymbolicName("Drought", "Drought")]
        Drought = 17,
        [SymbolicName("InfrastructureFailure", "Infrastructure Failure")]
        InfrastructureFailure = 18,
        [SymbolicName("NaturalDisaster", "Natural Disaster")]
        NaturalDisaster = 19,
        [SymbolicName("PublicHoliday", "Public Holiday")]
        PublicHoliday = 20,
        [SymbolicName("Terrorism", "Terrorist Attack")]
        Terrorism = 21,
        [SymbolicName("ColdWar", "Cold War")]
        ColdWar = 22,
        [SymbolicName("Colonisation", "Colonisation")]
        Colonisation = 23,
        [SymbolicName("HistoricEvent", "Historic Event")]
        HistoricEvent = 24,
        [SymbolicName("Revolution", "Revolution")]
        Revolution = 25,
        [SymbolicName("TechnologicalLeap", "Technological Leap")]
        TechnologicalLeap = 26,
        [SymbolicName("TradeWar", "Trade War")]
        TradeWar = 27,
    }

    public enum Happiness
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("$Faction_HappinessBand1;", "Elated")]
        Elated = 1,
        [SymbolicName("$Faction_HappinessBand2;", "Happy")]
        Happy = 2,
        [SymbolicName("$Faction_HappinessBand3;", "Discontented")]
        Discontented = 3,
        [SymbolicName("$Faction_HappinessBand4;", "Unhappy")]
        Unhappy = 4,
        [SymbolicName("$Faction_HappinessBand5;", "Despondent")]
        Despondent = 5,
    }

    public enum SecurityLevel
    {
        [SymbolicName("", "Unknown")]
        Unknown = 0,
        [SymbolicName("$GAlAXY_MAP_INFO_state_anarchy;", "Anarchy")]
        Anarchy = 1,
        [SymbolicName("$GALAXY_MAP_INFO_state_lawless;", "Lawless")]
        Lawless = 2,
        [SymbolicName("$SYSTEM_SECURITY_low;", "Low")]
        Low = 3,
        [SymbolicName("$SYSTEM_SECURITY_medium;", "Medium")]
        Medium = 4,
        [SymbolicName("$SYSTEM_SECURITY_high;", "High")]
        High = 5,
    }

    public enum FactionStateStage
    {
        Unknown = 0,
        Pending = 1,
        Active = 2,
        Recovering = 3,
    };

    public class Economy
    {
        public EconomyType Type { get; internal set; }
        public double? Proportion { get; internal set; }

        public static readonly Economy None = new Economy { Type = EconomyType.None, Proportion = 0.0 };
    }
}