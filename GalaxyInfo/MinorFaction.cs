using System;
using System.Collections.Generic;
using System.Linq;

namespace DangerousSprinkler
{
    public class MinorFaction
    {
        public string Name { get; internal set; }
        public Allegiance Allegiance { get; internal set; }
        public Government Government { get; internal set; }
        public bool? IsPlayerFaction { get; internal set; }
        public DateTimeOffset LastUpdateTime { get; internal set; }

        public class SystemStatus
        {
            public MinorFaction Faction { get; internal set; }
            public StarSystem System { get; internal set; }
            public List<StateInfo> ActiveStates { get; internal set; } = new();
            public List<StateInfo> PendingStates { get; internal set; } = new();
            public List<StateInfo> RecoveringStates { get; internal set; } = new();
            public DateTimeOffset LastUpdateTime { get; internal set; }
        }

        public class StateInfo
        {
            public FactionState State { get; internal set; }
            public DateTimeOffset FirstSeenAt { get; internal set; }
            public DateTimeOffset CurrentStageSeenAt { get; internal set; }
        }
    }
}