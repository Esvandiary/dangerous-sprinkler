using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;

namespace DangerousSprinkler
{
    public class StarSystemInfo
    {
        public ulong ID { get; internal set; }
        public string Name { get; internal set; }
        public Vector3 Position { get; internal set; }

        public double DistanceTo(StarSystemInfo other) => (Position - other.Position).Length();
    }

    public class StarSystem : StarSystemInfo
    {
        public Allegiance Allegiance { get; internal set; }
        public Government Government { get; internal set; }
        public EconomyType PrimaryEconomy { get; internal set; }
        public EconomyType SecondaryEconomy { get; internal set; }
        public SecurityLevel SecurityLevel { get; internal set; }
        public long Population { get; internal set; }

        public MinorFaction.SystemStatus ControllingFaction { get; internal set; }
        public List<MinorFaction.SystemStatus> Factions { get; internal set; }

        public List<IMarket> Markets { get; internal set; }
        public IEnumerable<Station> Stations { get => Markets?.OfType<Station>(); }
        public IEnumerable<FleetCarrier> FleetCarriers { get => Markets?.OfType<FleetCarrier>(); }

        public DateTimeOffset LastUpdateTime { get; internal set; }
    }
}