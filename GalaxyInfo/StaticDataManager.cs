﻿using System;
using System.Data.Common;
using System.Linq;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using DangerousSprinkler.DataSources;
using DangerousSprinkler.Extensions;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DangerousSprinkler
{
    public partial class StaticDataManager : SprinklerComponent, IServerHTTPEndpoint
    {
        public StaticDataManager(CoreServices cs, DBManager db)
            : base(cs, "StaticData", "Static Galaxy Data")
        {
            m_db = db;

            m_db.RegisterGenerateStep(GenerateDBTables);
            m_db.RegisterPostGenerateStep(GenerateDBIndexes);
        }

#region SprinklerComponent
        public override void Initialise()
        {
            base.Initialise();

            var commodities = FDevIDs.GetCommodities().Concat(FDevIDs.GetRareCommodities()).ToList();
            m_commoditiesIDMap = commodities.ToDictionary(t => t.ID, t => t);
            m_commoditiesSymbolMap = commodities.ToDictionary(t => t.SymbolicName.ToLowerInvariant(), t => t);

            var updatedAt = GetStatus(m_db.CreateConnection(), m_db).UpdatedAt;
            bool upToDate = (updatedAt.HasValue && updatedAt.Value >= (DateTimeOffset.UtcNow - TimeSpan.FromDays(2)));

            if (!upToDate)
                UpdateDB();
        }
#endregion

#region IServerHTTPEndpoint
        public ReadOnlyCollection<string> EndpointHTTPAddresses { get; } = new(new[] { "/data/commodities" });
        public string Name { get => ComponentName; }
        public string Description { get; } = "Endpoint to access static data";
        public async Task HTTPRequestReceived(HttpRequest request, HttpResponse response)
        {
            var commoditiesJson = JArray.FromObject(m_commoditiesIDMap.Values.ToArray());
            await response.WriteAsync(commoditiesJson.ToString(Formatting.None));
        }
#endregion

        public CommodityEntry GetCommodity(ulong id) => m_commoditiesIDMap?.GetValueOrDefault(id);
        public CommodityEntry GetCommodity(string symbol) => m_commoditiesSymbolMap?.GetValueOrDefault(symbol.ToLowerInvariant());


        private void GenerateDBTables(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateSystemsStatic);
            dbc.ExecuteCommand(DBTableSchemas.CreateStationsStatic);

            SetComponentInitialised(dbc, m_db, DateTime.UtcNow, null);
        }

        private void GenerateDBIndexes(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexSystemsStaticName);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexStationsStaticName);
            dbc.ExecuteCommand(DBTableSchemas.CreateIndexStationsStaticSystemID);
        }

        private void UpdateDB()
        {
            Logger.Info("Updating static data tables...");

            using (var dbc = m_db.CreateConnection())
            using (var tx = dbc.BeginTransaction())
            {
                using (var ssys_cmd = dbc.CreateCommand())
                using (var sstn_cmd = dbc.CreateCommand())
                using (var sys_cmd = dbc.CreateCommand())
                using (var stn_cmd = dbc.CreateCommand())
                using (var fc_cmd = dbc.CreateCommand())
                using (var faction_cmd = dbc.CreateCommand())
                using (var sfaction_cmd = dbc.CreateCommand())
                {
                    ssys_cmd.CommandText = @"
                        INSERT INTO systems_static
                            VALUES (@id64, @name, @pos_x, @pos_y, @pos_z)
                        ON CONFLICT (id) DO UPDATE SET
                            name = @name, pos_x = @pos_x, pos_y = @pos_y, pos_z = @pos_z";
                    m_db.DeriveParameters(ssys_cmd);
                    ssys_cmd.Prepare();

                    sys_cmd.CommandText = @"
                        INSERT INTO systems VALUES (
                            @id64, @updated_at, @faction, @allegiance, @government,
                            @economy1, @economy2, @security, @population)
                        ON CONFLICT (id) DO NOTHING";
                    m_db.DeriveParameters(sys_cmd);
                    sys_cmd.Prepare();

                    sstn_cmd.CommandText = @"
                        INSERT INTO stations_static
                            VALUES (@market_id, @name, @system, @updated_at, @pad, @planetary, @type, @distance)
                        ON CONFLICT (market_id) DO UPDATE SET
                            name = @name, system_id = @system, updated_at = @updated_at,
                            max_pad_size = @pad, is_planetary = @planetary, type = @type, sc_distance = @distance";
                    m_db.DeriveParameters(sstn_cmd);
                    sstn_cmd.Prepare();

                    stn_cmd.CommandText = @"
                        INSERT INTO stations VALUES (
                            @market_id, @info_updated_at, NULL, @faction, @allegiance, @government,
                            @economy1, NULL, @economy2, NULL, @market, @shipyard, @outfitting, @blackmarket)
                        ON CONFLICT (market_id) DO NOTHING";
                    m_db.DeriveParameters(stn_cmd);
                    stn_cmd.Prepare();

                    fc_cmd.CommandText = @"
                        INSERT INTO fleetcarriers VALUES (
                            @market_id, @carrier_id, @system_id, NULL, @info_updated_at, NULL,
                            @distance, @market, @shipyard, @outfitting, @blackmarket)
                        ON CONFLICT (market_id) DO NOTHING";
                    m_db.DeriveParameters(fc_cmd);
                    fc_cmd.Prepare();

                    faction_cmd.CommandText = @"
                        INSERT INTO factions
                            VALUES (@name, @updated_at, @allegiance, @government, @pmf)
                        ON CONFLICT (name) DO NOTHING";
                    m_db.DeriveParameters(faction_cmd);
                    faction_cmd.Prepare();

                    sfaction_cmd.CommandText = @"
                        INSERT INTO system_factions
                            VALUES (@system, @faction, @updated_at, @influence, @happiness)
                        ON CONFLICT (system_id, faction) DO NOTHING";
                    m_db.DeriveParameters(sfaction_cmd);
                    sfaction_cmd.Prepare();

                    foreach (var s in EDSM.GetPopulatedSystems())
                    {
                        ssys_cmd.Parameters["@id64"].Value = (long)s.ID64;
                        ssys_cmd.Parameters["@name"].Value = s.Name;
                        ssys_cmd.Parameters["@pos_x"].Value = Coordinates.X.ToInternal(s.Location.X);
                        ssys_cmd.Parameters["@pos_y"].Value = Coordinates.Y.ToInternal(s.Location.Y);
                        ssys_cmd.Parameters["@pos_z"].Value = Coordinates.Z.ToInternal(s.Location.Z);
                        ssys_cmd.ExecuteNonQuery();

                        if (s.Factions != null)
                        {
                            foreach (var f in s.Factions)
                            {
                                faction_cmd.Parameters["@name"].Value = f.Name;
                                faction_cmd.Parameters["@updated_at"].Value = f.UpdatedAt.UtcDateTime;
                                faction_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromLocalisedName<Allegiance>(f.Allegiance);
                                faction_cmd.Parameters["@government"].Value = (int)Identifier.FromLocalisedName<Government>(f.Government);
                                faction_cmd.Parameters["@pmf"].Value = f.IsPlayerFaction;
                                faction_cmd.ExecuteNonQuery();

                                sfaction_cmd.Parameters["@system"].Value = (long)s.ID64;
                                sfaction_cmd.Parameters["@faction"].Value = f.Name;
                                sfaction_cmd.Parameters["@updated_at"].Value = f.UpdatedAt.UtcDateTime;
                                sfaction_cmd.Parameters["@influence"].Value = f.Influence;
                                sfaction_cmd.Parameters["@happiness"].Value = (int)Identifier.FromLocalisedName<Happiness>(f.Happiness);
                                sfaction_cmd.ExecuteNonQuery();
                            }
                        }

                        sys_cmd.Parameters["@id64"].Value = (long)s.ID64;
                        sys_cmd.Parameters["@updated_at"].Value = DateTime.UtcNow;
                        sys_cmd.Parameters["@faction"].SetValueOrNull(s.ControllingFaction?.Name);
                        sys_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromLocalisedName<Allegiance>(s.Allegiance);
                        sys_cmd.Parameters["@government"].Value = (int)Identifier.FromLocalisedName<Government>(s.Government);
                        sys_cmd.Parameters["@economy1"].Value = (int)Identifier.FromLocalisedName<EconomyType>(s.Economy);
                        sys_cmd.Parameters["@economy2"].Value = (int)EconomyType.None; // TODO: is this always true?
                        sys_cmd.Parameters["@security"].Value = (int)Identifier.FromLocalisedName<SecurityLevel>(s.Security);
                        sys_cmd.Parameters["@population"].SetValueOrNull(s.Population);
                        sys_cmd.ExecuteNonQuery();

                        if (s.Stations != null)
                        {
                            foreach (var st in s.Stations)
                            {
                                StationInfo stype = StationInfo.FromName(st.Type);
                                if (stype == null)
                                {
                                    Logger.Error($"Unknown station type \"{st.Type}\"");
                                    continue;
                                }
                                if (stype.IsFleetCarrier)
                                {
                                    fc_cmd.Parameters["@market_id"].Value = (long)st.MarketID;
                                    fc_cmd.Parameters["@carrier_id"].Value = st.Name;
                                    fc_cmd.Parameters["@system_id"].Value = (long)s.ID64;
                                    fc_cmd.Parameters["@info_updated_at"].Value = st.UpdatedAt.Information.UtcDateTime;
                                    fc_cmd.Parameters["@distance"].SetValueOrNull((long?)st.DistanceFromStar);
                                    fc_cmd.Parameters["@market"].Value = st.HasMarket;
                                    fc_cmd.Parameters["@shipyard"].Value = st.HasShipyard;
                                    fc_cmd.Parameters["@outfitting"].Value = st.HasOutfitting;
                                    fc_cmd.Parameters["@blackmarket"].Value = st.OtherServices.Contains("Black Market");
                                    fc_cmd.ExecuteNonQuery();
                                }
                                else
                                {
                                    sstn_cmd.Parameters["@market_id"].Value = (long)st.MarketID;
                                    sstn_cmd.Parameters["@name"].Value = st.Name;
                                    sstn_cmd.Parameters["@system"].Value = (long)s.ID64;
                                    sstn_cmd.Parameters["@updated_at"].Value = st.UpdatedAt.Information.UtcDateTime;
                                    sstn_cmd.Parameters["@pad"].Value = Identifier.ToSymbolicName(stype.MaxPadSize);
                                    sstn_cmd.Parameters["@planetary"].Value = stype.IsPlanetary;
                                    sstn_cmd.Parameters["@type"].Value = st.Type;
                                    sstn_cmd.Parameters["@distance"].SetValueOrNull((long?)st.DistanceFromStar);
                                    sstn_cmd.ExecuteNonQuery();

                                    stn_cmd.Parameters["@market_id"].Value = (long)st.MarketID;
                                    stn_cmd.Parameters["@info_updated_at"].Value = st.UpdatedAt.Information.UtcDateTime;
                                    stn_cmd.Parameters["@faction"].SetValueOrNull(st.ControllingFaction?.Name);
                                    stn_cmd.Parameters["@allegiance"].Value = (int)Identifier.FromLocalisedName<Allegiance>(st.Allegiance);
                                    stn_cmd.Parameters["@government"].Value = (int)Identifier.FromLocalisedName<Government>(st.Government);
                                    stn_cmd.Parameters["@economy1"].Value = (int)Identifier.FromLocalisedNameOrDefault<EconomyType>(st.PrimaryEconomy, EconomyType.None);
                                    stn_cmd.Parameters["@economy2"].Value = (int)Identifier.FromLocalisedNameOrDefault<EconomyType>(st.SecondaryEconomy, EconomyType.None);
                                    stn_cmd.Parameters["@market"].Value = st.HasMarket;
                                    stn_cmd.Parameters["@shipyard"].Value = st.HasShipyard;
                                    stn_cmd.Parameters["@outfitting"].Value = st.HasOutfitting;
                                    stn_cmd.Parameters["@blackmarket"].Value = st.OtherServices.Contains("Black Market");
                                    stn_cmd.ExecuteNonQuery();
                                }
                            }
                        }
                    }
                }

                SetUpdatedAt(dbc, m_db, DateTime.UtcNow);

                tx.Commit();
            }
            Logger.Info("Done updating static data tables.");
        }

        private DBManager m_db;
        private Dictionary<ulong, CommodityEntry> m_commoditiesIDMap;
        private Dictionary<string, CommodityEntry> m_commoditiesSymbolMap;
    }
}
