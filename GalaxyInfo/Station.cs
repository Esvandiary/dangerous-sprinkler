using System;

namespace DangerousSprinkler
{
    public interface IMarket
    {
        ulong MarketID { get; }
        string Name { get; }
        StarSystem System { get; }
        PadSize MaxPadSize { get; }
        uint? DistanceFromEntryPoint { get; }
        bool IsPlanetary { get; }
        StationType Type { get; }
        Government Government { get; }
        Economy PrimaryEconomy { get; }
        Economy SecondaryEconomy { get; }
        bool HasMarket { get; }
        bool HasShipyard { get; }
        bool HasOutfitting { get; }
        bool HasBlackMarket { get; }
        DateTimeOffset LastUpdateTime { get; }
        DateTimeOffset? MarketUpdateTime { get; }
    }

    public class Station : IMarket
    {
        public ulong MarketID { get; internal set; }
        public string Name { get; internal set; }
        public StarSystem System { get; internal set; }
        public PadSize MaxPadSize { get; internal set; }
        public uint? DistanceFromEntryPoint { get; internal set; }
        public bool IsPlanetary { get; internal set; }
        public StationType Type { get; internal set; }
        public MinorFaction.SystemStatus ControllingFaction { get; internal set; }
        public Allegiance Allegiance { get; internal set; }
        public Government Government { get; internal set; }
        public Economy PrimaryEconomy { get; internal set; }
        public Economy SecondaryEconomy { get; internal set; }
        public bool HasMarket { get; internal set; }
        public bool HasShipyard { get; internal set; }
        public bool HasOutfitting { get; internal set; }
        public bool HasBlackMarket { get; internal set; }

        public DateTimeOffset LastUpdateTime { get; internal set; }
        public DateTimeOffset? MarketUpdateTime { get; internal set; }
    }

    public class FleetCarrier : IMarket
    {
        public ulong MarketID { get; internal set; }
        public string Name { get; internal set; }
        public string CarrierName { get; internal set; }
        public StarSystem System { get; internal set; }
        public PadSize MaxPadSize { get => PadSize.Large; }
        public uint? DistanceFromEntryPoint { get; internal set; }
        public bool IsPlanetary { get => false; }
        public StationType Type { get => StationType.FleetCarrier; }
        public Government Government { get => Government.Carrier; }
        public Economy PrimaryEconomy { get; } = new Economy { Type = EconomyType.PrivateEnterprise, Proportion = 1.0 };
        public Economy SecondaryEconomy { get; } = Economy.None;
        public bool HasMarket { get; internal set; }
        public bool HasShipyard { get; internal set; }
        public bool HasOutfitting { get; internal set; }
        public bool HasBlackMarket { get; internal set; }

        public DateTimeOffset LastUpdateTime { get; internal set; }
        public DateTimeOffset? MarketUpdateTime { get; internal set; }
    }
}