using System;
using System.IO;
using System.Net.Http;
using System.Collections.Generic;
using System.Linq;
using System.Timers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using H.Socket.IO;

namespace DangerousSprinkler
{
    public interface ITickTracker : IDisposable
    {
        void Start();
        void Stop();
        DateTimeOffset GetLatestTickTimeBefore(DateTimeOffset time);
        DateTimeOffset LatestTickTime { get; }
        DateTimeOffset NextPredictedTickTime { get; }
        TimeSpan ExpectedUpdateDelay { get; set; }
        TimeSpan TickCacheDuration { get; set; }
    }

    internal class PhelboreTickTracker : ITickTracker
    {
        private static readonly string TickFeedURL = "ws://tick.phelbore.com:31173";

        public PhelboreTickTracker(Logger logger)
        {
            m_logger = logger;
            m_sio.On("message", this.OnTickReceived);
        }

        public void Start()
        {
            m_sio.ConnectAsync(new Uri(TickFeedURL)).Wait();
        }

        public void Stop()
        {
            try { m_sio.DisconnectAsync().Wait(); } catch (Exception) {}
        }

        private void OnTickReceived(string msg) => AddTick(msg);

        private void AddTick(string d)
        {
            try
            {
                DateTimeOffset date = DateTimeUtil.FromISO8601(d);
                DateTimeOffset cacheThreshold = DateTime.UtcNow - TickCacheDuration;
                lock (m_lock)
                {
                    m_ticks.RemoveAll(t => t < cacheThreshold);
                    m_ticks.Add(date);
                }
                m_logger.Info($"New tick at {LatestTickTime.ToString("o")}");
            }
            catch (Exception ex)
            {
                m_logger.Error($"EXCEPTION while adding new tick date: {ex.Message}");
            }
        }

        public DateTimeOffset GetLatestTickTimeBefore(DateTimeOffset time)
        {
            lock (m_lock)
                return m_ticks.Where(t => t < time).Max();
        }

        public DateTimeOffset LatestTickTime { get => GetLatestTickTimeBefore(DateTimeOffset.UtcNow); }
        public DateTimeOffset NextPredictedTickTime { get => LatestTickTime + TimeSpan.FromDays(1); }

        public TimeSpan ExpectedUpdateDelay { get; set; } = TimeSpan.FromMinutes(15);
        public TimeSpan TickCacheDuration { get; set; } = TimeSpan.FromDays(7);

        public void Dispose() => Dispose(true);
        private void Dispose(bool disposing)
        {
            if (disposing)
                Stop();
        }

        private Logger m_logger;
        private SocketIoClient m_sio = new();
        private List<DateTimeOffset> m_ticks = new List<DateTimeOffset>();
        private object m_lock = new object();
    }

    internal class EBGSTickTracker : ITickTracker
    {
        private static readonly string BGSTicksURL = "https://elitebgs.app/api/ebgs/v5/ticks";

        public EBGSTickTracker(Logger logger)
        {
            m_logger = logger;
            m_timer.Elapsed += DoPeriodicUpdate;
        }

        public void Start()
        {
            DoPeriodicUpdate(this, null);
        }

        public void Stop()
        {
            lock (m_lock)
            {
                m_timer?.Stop();
            }
        }

        public DateTimeOffset GetLatestTickTimeBefore(DateTimeOffset time)
            => m_ticks.Where(t => t < time).Max();

        public DateTimeOffset LatestTickTime { get => GetLatestTickTimeBefore(DateTimeOffset.UtcNow); }
        public DateTimeOffset NextPredictedTickTime { get => LatestTickTime + TimeSpan.FromDays(1); }

        public TimeSpan ExpectedUpdateDelay { get; set; } = TimeSpan.FromMinutes(15);
        public TimeSpan UpdateRepeatCheckInterval { get; set; } = TimeSpan.FromMinutes(1);
        public TimeSpan TickCacheDuration { get; set; } = TimeSpan.FromDays(7);

        public void Dispose() => Dispose(true);
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                Stop();
                m_timer?.Dispose();
                m_timer = null;
            }
        }

        private void DoPeriodicUpdate(object sender, ElapsedEventArgs e)
        {
            m_logger.Info("Going to check for new BGS tick...");
            bool foundNewTick = UpdateTicks();
            double interval = (foundNewTick)
                ? (NextPredictedTickTime + ExpectedUpdateDelay - DateTimeOffset.UtcNow).TotalMilliseconds
                : UpdateRepeatCheckInterval.TotalMilliseconds;
            m_logger.Info(
                (foundNewTick ? $"New tick at {LatestTickTime.ToString("o")}" : "No new tick")
                + $", next check in {(long)(interval / 1000)}s.");
            lock (m_lock)
            {
                m_timer.Interval = interval;
                m_timer.Start();
            }
        }

        private bool UpdateTicks()
        {
            DateTimeOffset? oldLatest = (m_ticks.Count != 0) ? LatestTickTime : null;

            long timeMin = (DateTimeOffset.UtcNow - TickCacheDuration).ToUnixTimeMilliseconds();
            long timeMax = DateTimeOffset.UtcNow.ToUnixTimeMilliseconds();
            string url = $"{BGSTicksURL}?timeMin={timeMin}&timeMax={timeMax}";

            using (var client = new HttpClient())
            using (var req = new HttpRequestMessage(HttpMethod.Get, url))
            {
                var resp = client.Send(req);
                if (resp.IsSuccessStatusCode)
                {
                    using (var sr = new StreamReader(resp.Content.ReadAsStream()))
                    using (var jtr = new JsonTextReader(sr))
                    {
                        var data = JArray.ReadFrom(jtr);
                        var times = data.Children<JObject>()
                            .Select(t => DateTimeUtil.FromISO8601(t.Value<string>("time"))).ToList();
                        m_ticks = times;
                        return (!oldLatest.HasValue || times.Max() > oldLatest.Value);
                    }
                }
                else
                {
                    m_logger.Error($"Failed to get ticks: HTTP response {resp.StatusCode}");
                    return false;
                }
            }
        }

        private Timer m_timer = new Timer { AutoReset = false };
        private IReadOnlyList<DateTimeOffset> m_ticks = new List<DateTimeOffset>();
        private Logger m_logger;
        private object m_lock = new object();
    }
}