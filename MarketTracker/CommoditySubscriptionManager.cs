﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using Newtonsoft.Json.Linq;
using DangerousSprinkler.Extensions;
using DangerousSprinkler.EDDN;
using Newtonsoft.Json;

namespace DangerousSprinkler
{
    internal class LiveCommoditySubscription : LiveCommoditySubscriptionInfo
    {
        [JsonIgnore]
        public Client Client { get; set; }
        [JsonIgnore]
        public StarSystemInfo ReferenceSystemInfo { get; set; }
    }

    internal class CommoditySubscriptionManager
    {
        public CommodityEntry Commodity { get; private set; }
        public CommoditySubscriptionManager(DBManager dbm, GalaxyInfo gi, CommodityEntry ce)
        {
            m_gi = gi;
            Commodity = ce;

            using (var dbc = dbm.CreateConnection())
            using (var cmd = dbc.CreateCommand())
            {
                cmd.CommandText = @"
                    SELECT
                        sys.name sy_name, sys.id sy_id,
                        st_name, st_type, mp.market_id market_id,
                        max_pad_size, sc_distance,
                        mp.sell_price sell_price, mp.demand demand, mp.demand_bracket dbracket,
                        market_updated_at
                    FROM market_prices mp
                    INNER JOIN
                    (
                        SELECT
                            sts.market_id market_id, name st_name, type st_type,
                            system_id, max_pad_size, sc_distance, market_updated_at
                        FROM stations_static sts INNER JOIN stations stx ON sts.market_id = stx.market_id
                        UNION
                        SELECT
                            market_id market_id, carrier_id st_name, 'FleetCarrier' st_type,
                            system_id, 'L' max_pad_size, sc_distance, market_updated_at
                        FROM fleetcarriers
                    ) st
                    ON mp.market_id = st.market_id
                    INNER JOIN systems_static sys ON sys.id = st.system_id
                    WHERE mp.commodity_id = @commodity";
                dbm.DeriveParameters(cmd);
                cmd.Parameters["@commodity"].Value = (long)ce.ID;
                using (var reader = cmd.ExecuteReader())
                while (reader.Read())
                {
                    ulong id64 = (ulong)reader.GetInt64("sy_id");
                    var stationType = StationInfo.FromName(reader.Maybe("st_type", reader.GetString, ""))?.Type;
                    m_currentState[id64] = new LiveCommodityUpdate
                    {
                        CommodityName = ce.SymbolicName,
                        AboveMinPrice = false, // placeholder
                        SystemName = reader.GetString("sy_name"),
                        SystemID = id64,
                        DistanceToRefSystem = 0.0, // placeholder
                        StationName = reader.GetString("st_name"),
                        StationType = Identifier.ToSymbolicName(stationType ?? StationType.Unknown),
                        MarketID = (ulong)reader.GetInt64("market_id"),
                        MaxPadSize = reader.GetString("max_pad_size"),
                        DistanceFromStar = (ulong)reader.Maybe("sc_distance", reader.GetInt64, 0),
                        SellPrice = reader.GetInt64("sell_price"),
                        Demand = reader.GetInt64("demand"),
                        DemandBracket = Enum.GetName((CommodityBracket)reader.GetInt32("dbracket")),
                        UpdatedAt = reader.GetUTCDateTimeOffset("market_updated_at").ToString("o")
                    };
                }
            }
        }

        public void HandleCommodityUpdate(CommodityUpdate u, CommodityUpdateEntry c)
        {
            var s = m_gi.GetSystems(u.SystemName).FirstOrDefault(s => s.Stations.Any(st => st.MarketID == u.MarketID));
            var st = s?.Stations?.FirstOrDefault(st => st.MarketID == u.MarketID);
            if (s == null || st == null) return;

            string demandBracket = c.DemandBracket.ToString();
            string updatedAt = DateTimeUtil.ToISO8601(u.UpdatedAt);

            LiveCommodityUpdate lu = new LiveCommodityUpdate
            {
                CommodityName = c.SymbolicName,
                AboveMinPrice = false, // placeholder
                SystemName = s.Name,
                SystemID = s.ID,
                DistanceToRefSystem = 0.0, // placeholder
                StationName = st.Name,
                StationType = Identifier.ToSymbolicName(st.Type),
                MarketID = st.MarketID,
                MaxPadSize = Identifier.ToSymbolicName(st.MaxPadSize),
                DistanceFromStar = st.DistanceFromEntryPoint ?? 0, // hack
                SellPrice = c.SellPrice,
                Demand = c.Demand,
                DemandBracket = demandBracket,
                UpdatedAt = updatedAt,
            };
            LiveCommodityUpdate old_lu = m_currentState.GetValueOrDefault(lu.MarketID);
            m_currentState[lu.MarketID] = lu;

            lock (m_subscriptions)
            {
                foreach (var sub in m_subscriptions)
                    ConsiderSendCommodityUpdate(sub, lu, s, old_lu);
            }
        }

        private void ConsiderSendCommodityUpdate(LiveCommoditySubscription sub, LiveCommodityUpdate lu, StarSystem s, LiveCommodityUpdate old_lu)
        {
            bool above_min_price = (lu.SellPrice >= sub.MinPrice);
            bool old_above_min = (old_lu != null && old_lu.SellPrice >= sub.MinPrice);

            if (above_min_price || old_above_min)
            {
                // get system info if we didn't already
                s ??= m_gi.GetSystems(lu.SystemName).FirstOrDefault(s => s.Stations.Any(st => st.MarketID == lu.MarketID));
                if (s == null) return;
                double dist_to_ref = s.DistanceTo(sub.ReferenceSystemInfo);
                // TODO: check all requirements

                var clu = lu.Clone(above_min_price, dist_to_ref);
                _ = sub.Client.SendAsync("commodity", JObject.FromObject(clu));
            }
        }

        public void Subscribe(LiveCommoditySubscription info)
        {
            lock (m_subscriptions)
                m_subscriptions.Add(info);
            // send any current matching stations
            foreach (var lu in m_currentState.Values)
                ConsiderSendCommodityUpdate(info, lu, null, null);
        }

        public void Unsubscribe(Client client)
        {
            lock (m_subscriptions)
                m_subscriptions.RemoveAll(t => t.Client == client);
        }

        private GalaxyInfo m_gi;
        private List<LiveCommoditySubscription> m_subscriptions = new();
        private ConcurrentDictionary<ulong, LiveCommodityUpdate> m_currentState = new();
    }
}
