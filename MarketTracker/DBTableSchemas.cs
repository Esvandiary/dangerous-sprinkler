namespace DangerousSprinkler
{
    public partial class MarketTracker
    {
        public static class DBTableSchemas
        {
            public static readonly string CreateMarketPrices = @"
                CREATE TABLE market_prices (
                    commodity_id BIGINT NOT NULL,
                    market_id BIGINT NOT NULL,
                    sell_price BIGINT NOT NULL,
                    demand BIGINT NOT NULL,
                    demand_bracket INT NOT NULL,
                    buy_price BIGINT NOT NULL,
                    supply BIGINT NOT NULL,
                    supply_bracket INT NOT NULL,
                    PRIMARY KEY (commodity_id, market_id)
                )
            ";
        }
    }
}