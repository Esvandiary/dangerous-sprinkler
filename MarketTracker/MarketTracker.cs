﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Common;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using DangerousSprinkler.Extensions;
using DangerousSprinkler.EDDN;

namespace DangerousSprinkler
{
    public partial class MarketTracker : SprinklerComponent, IServerWSEndpoint, IReceiverListener
    {
        public MarketTracker(CoreServices cs, DBManager db, GalaxyInfo gi)
            : base(cs, "MarketTracker", "Market Tracker")
        {
            m_processingThread = new Thread(RunProcessingThread);
            m_db = db;
            m_gi = gi;

            m_db.RegisterGenerateStep(GenerateDBTables);
        }

        private Task HandleConfigMessage(Client client, string message, JObject content)
        {
            // TODO: some config
            return Task.CompletedTask;
        }

        private Task HandleSubscribeMessage(Client client, string message, JObject content)
        {
            LiveCommoditySubscription s = content.ToObject<LiveCommoditySubscription>();
            CommodityEntry ce = m_gi.GetCommodity(s.Name);
            if (ce == null) return Task.CompletedTask;

            s.Client = client;
            s.ReferenceSystemInfo = m_gi.GetSystemInfos(s.ReferenceSystemName).FirstOrDefault();
            if (s.ReferenceSystemInfo == null) return Task.CompletedTask; // TODO: error

            string name = ce.SymbolicName.ToLowerInvariant();
            if (!m_liveSubscriptions.ContainsKey(name))
                m_liveSubscriptions[name] = new CommoditySubscriptionManager(m_db, m_gi, ce);
            m_liveSubscriptions[name].Unsubscribe(client);
            m_liveSubscriptions[name].Subscribe(s);
            Logger.Debug($"subscribed to {name}");
            return Task.CompletedTask;
        }

        private Task HandleUnsubscribeMessage(Client client, string message, JObject content)
        {
            string name = content.Value<string>("commodity_name");
            if (!m_liveSubscriptions.ContainsKey(name)) return Task.CompletedTask;
            m_liveSubscriptions[name].Unsubscribe(client);
            return Task.CompletedTask;
        }

#region IServerWSEndpoint
        public ReadOnlyCollection<string> EndpointWSAddresses { get; } = new(new[] { "/api/markets" });
        public string Name { get; } = "Market Updates";
        public string Description { get; } = "Provides updates on commodity market and black market prices";

        public Task WSClientConnected(Client client, string endpoint)
        {
            client.SetMessageHandler("config", HandleConfigMessage);
            client.SetMessageHandler("subscribe", HandleSubscribeMessage);
            client.SetMessageHandler("unsubscribe", HandleUnsubscribeMessage);
            return Task.CompletedTask;
        }

        public Task WSClientDisconnected(Client client, string endpoint)
        {
            foreach (var list in m_liveSubscriptions.Values)
                list.Unsubscribe(client);
            return Task.CompletedTask;
        }
#endregion

#region IReceiverListener
        public ReadOnlyCollection<string> SchemaRefs { get; } = new(new[]{ SchemaURLs.Commodity_v30, SchemaURLs.BlackMarket_v10 });
        public BlockingCollection<JObject> Queue { get; } = new(256);
#endregion

#region SprinklerComponent
        public override void Start()
        {
            base.Start();
            m_processingThread.Start();
        }

        public override void Stop()
        {
            base.Stop();
            if (m_processingThread.IsAlive)
                m_processingThread.Join();
        }
#endregion

        private void RunProcessingThread()
        {
            while (IsRunning)
            {
                JObject o = null;
                while (IsRunning && !Queue.TryTake(out o, 250));
                if (!IsRunning) break;
                if (o.Value<string>("$schemaRef") == SchemaURLs.Commodity_v30)
                {
                    CommodityUpdate u = o["message"].ToObject<CommodityUpdate>();
                    Logger.Debug($"Got commodity update for {u.SystemName}/{u.StationName}");
                    HandleCommodityUpdate(u);
                }
            }
        }

        private void HandleCommodityUpdate(CommodityUpdate u)
        {
            foreach (var c in u.Commodities)
            {
                m_liveSubscriptions.GetValueOrDefault(c.SymbolicName.ToLowerInvariant())?.HandleCommodityUpdate(u, c);
            }

            using (var dbc = m_db.CreateConnection())
            using (var tx = dbc.BeginTransaction())
            {
                int rowsUpdated = 0;
                using (var st_cmd = dbc.CreateCommand())
                {
                    st_cmd.CommandText = @"
                        UPDATE stations SET market_updated_at = @time
                        WHERE market_id = @market_id AND (market_updated_at IS NULL OR market_updated_at < @time)";
                    m_db.DeriveParameters(st_cmd);
                    st_cmd.Parameters["@market_id"].Value = (long)u.MarketID;
                    st_cmd.Parameters["@time"].Value = u.UpdatedAt.UtcDateTime;
                    rowsUpdated += st_cmd.ExecuteNonQuery();
                }
                using (var fc_cmd = dbc.CreateCommand())
                {
                    fc_cmd.CommandText = @"
                        UPDATE fleetcarriers SET market_updated_at = @time
                        WHERE market_id = @market_id AND (market_updated_at IS NULL OR market_updated_at < @time)";
                    m_db.DeriveParameters(fc_cmd);
                    fc_cmd.Parameters["@market_id"].Value = (long)u.MarketID;
                    fc_cmd.Parameters["@time"].Value = u.UpdatedAt.UtcDateTime;
                    rowsUpdated += fc_cmd.ExecuteNonQuery();
                }

                if (rowsUpdated > 0)
                {
                    using (var mp_cmd = dbc.CreateCommand())
                    {
                        mp_cmd.CommandText = @"
                            INSERT INTO market_prices VALUES
                                (@comm_id, @market_id, @sprice, @demand, @demandb, @bprice, @supply, @supplyb)
                            ON CONFLICT (commodity_id, market_id) DO UPDATE SET
                                sell_price = @sprice, demand = @demand, demand_bracket = @demandb,
                                buy_price = @bprice, supply = @supply, supply_bracket = @supplyb";
                        m_db.DeriveParameters(mp_cmd);
                        mp_cmd.Prepare();

                        foreach (var c in u.Commodities)
                        {
                            CommodityEntry ce = m_gi.GetCommodity(c.SymbolicName);
                            if (ce == null)
                            {
                                Logger.Error($"Unknown commodity {c.SymbolicName}");
                                continue;
                            }
                            mp_cmd.Parameters["@comm_id"].Value = (long)ce.ID;
                            mp_cmd.Parameters["@market_id"].Value = (long)u.MarketID;
                            mp_cmd.Parameters["@sprice"].Value = c.SellPrice;
                            mp_cmd.Parameters["@demand"].Value = c.Demand;
                            mp_cmd.Parameters["@demandb"].Value = (int)c.DemandBracket;
                            mp_cmd.Parameters["@bprice"].Value = c.BuyPrice;
                            mp_cmd.Parameters["@supply"].Value = c.Supply;
                            mp_cmd.Parameters["@supplyb"].Value = (int)c.SupplyBracket;
                            mp_cmd.ExecuteNonQuery();
                        }
                    }

                    using (var mpd_cmd = dbc.CreateCommand())
                    {
                        mpd_cmd.CommandText = @"
                            DELETE FROM market_prices
                                WHERE market_id = @market_id AND commodity_id NOT IN (SELECT unnest(@commodities::bigint[]))";
                        m_db.DeriveParameters(mpd_cmd);
                        mpd_cmd.Parameters["@market_id"].Value = (long)u.MarketID;
                        mpd_cmd.Parameters["@commodities"].Value
                            = u.Commodities.Select(c => m_gi.GetCommodity(c.SymbolicName))
                                .Where(ce => ce != null).Select(ce => (long)ce.ID).ToArray();
                        mpd_cmd.ExecuteNonQuery();
                    }
                }

                tx.Commit();
            }
        }

        private void GenerateDBTables(DbConnection dbc, IDBDeriveParameters dbm)
        {
            dbc.ExecuteCommand(DBTableSchemas.CreateMarketPrices);

            SetComponentInitialised(dbc, dbm, DateTime.UtcNow, null);
        }

        private Thread m_processingThread;
        private DBManager m_db;
        private GalaxyInfo m_gi;
        private Dictionary<string, CommoditySubscriptionManager> m_liveSubscriptions = new();
    }
}
