# Dangerous Sprinkler

EDDN/EDSM-data/Postgres/WebSocket mashup.

## Getting Started

This project requires a [.NET SDK](https://dotnet.microsoft.com/download/dotnet) >= [5.0](https://dotnet.microsoft.com/download/dotnet/5.0) installed, either standalone or as part of Visual Studio 2019 or later.

To build and run the project:

```cs
dotnet restore
dotnet build
dotnet run -p Server
```

This should start a localhost webserver running on port 5000.