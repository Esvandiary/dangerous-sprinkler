using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.WebSockets;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using DangerousSprinkler;

namespace DangerousSprinkler.Server
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IHostApplicationLifetime lifetime)
        {
            lifetime.ApplicationStopping.Register(OnShutdown);

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }

            app.UseBlazorFrameworkFiles();
            app.UseDefaultFiles();
            app.UseStaticFiles(new StaticFileOptions
            {
                OnPrepareResponse = ctx =>
                {
                    ctx.Context.Response.Headers.Append(
                        "Cache-Control", $"public, max-age={StaticFileCacheDuration.TotalSeconds:F0}");
                }
            });
            app.UseRouting();
            app.UseWebSockets();

            lock (m_initLock)
            {
                m_cs = new CoreServices
                {
                    Config = Config.Load("dsconfig.json"),
                    Logging = new Logging(app.ApplicationServices.GetRequiredService<ILoggerFactory>()),
                    Metrics = new Metrics()
                };

                m_logger = m_cs.Logging.GetLogger("Server");

                // initialise external connections
                m_receiver = new EDDNReceiver(
                    m_cs.Config.Get<string>("eddn.host"),
                    m_cs.Config.Get<int>("eddn.port"),
                    m_cs.Logging.GetLogger<EDDNReceiver>());
                m_db = new DBManager(m_cs);

                // order is important, these will be torn down in reverse order
                var sdm = new StaticDataManager(m_cs, m_db);
                var bgs = new BGSTracker(m_cs, m_db);
                var ginfo = new GalaxyInfo(m_cs, m_db, sdm, bgs);
                m_components.Add(m_db);
                m_components.Add(sdm);
                m_components.Add(ginfo);
                m_components.Add(bgs);
                m_components.Add(new MarketTracker(m_cs, m_db, ginfo));
                m_components.Add(new StationTracker(m_cs, m_db, ginfo));
                m_components.Add(new UploaderCounter(m_cs));

                foreach (var c in m_components)
                    c.Initialise();

                foreach (var c in m_components)
                    c.Start();

                // start connections listening
                foreach (var c in m_components.OfType<IReceiverListener>())
                    m_receiver.AddListener(c);
            }

            m_receiver.Connect();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");

                foreach (var ep in m_components.OfType<IServerHTTPEndpoint>())
                {
                    foreach (var epaddr in ep.EndpointHTTPAddresses)
                    {
                        endpoints.MapGet(epaddr, context =>
                        {
                            try
                            {
                                ep.HTTPRequestReceived(context.Request, context.Response);
                            }
                            catch (Exception ex)
                            {
                                m_logger.Error(ex, $"Exception in HTTP request handler: {ex.Message}");
                                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                            }
                            return Task.CompletedTask;
                        });
                    }
                }

                foreach (var ep in m_components.OfType<IServerWSEndpoint>())
                {
                    foreach (var epaddr in ep.EndpointWSAddresses)
                    {
                        endpoints.MapGet(epaddr, async context =>
                        {
                            if (context.WebSockets.IsWebSocketRequest)
                            {
                                WebSocket ws = await context.WebSockets.AcceptWebSocketAsync();
                                Client client = new Client(ws, m_cs.Logging.GetLogger<Client>());
                                lock (m_clients)
                                    m_clients.Add(client);
                                await ep.WSClientConnected(client, epaddr);

                                string msg = null;
                                try
                                {
                                    while ((msg = await client.ReceiveTextAsync()) != null)
                                    {
                                        await client.AttemptDispatch(msg);
                                    }
                                }
                                catch (Exception ex)
                                {
                                    m_logger.Error(ex, $"Exception in WebSocket read handler: {ex.Message}");
                                }

                                await ep.WSClientDisconnected(client, epaddr);
                                try { await client.CloseAsync("socket closed"); } catch (Exception) {}
                                lock (m_clients)
                                    m_clients.Remove(client);
                                client.Dispose();
                            }
                            else
                            {
                                context.Response.StatusCode = StatusCodes.Status400BadRequest;
                            }
                        });
                    }
                }
            });
        }

        private void OnShutdown()
        {
            lock (m_initLock)
            {
                lock (m_clients)
                {
                    for (int i = m_clients.Count - 1; i >= 0; --i)
                    {
                        try { m_clients[i].CloseAsync("application shutting down").Wait(); } catch (Exception) {}
                        m_clients[i].Dispose();
                    }
                    m_clients.Clear();
                }

                for (int i = m_components.Count - 1; i >= 0; --i)
                {
                    m_components[i].Stop();
                    m_components[i].Dispose();
                }
                m_components.Clear();

                m_receiver?.Disconnect();
                m_receiver?.Dispose();
                m_receiver = null;
            }
        }


        private CoreServices m_cs;
        private Logger m_logger;
        private DBManager m_db;
        private EDDNReceiver m_receiver;
        private List<SprinklerComponent> m_components = new List<SprinklerComponent>();
        private List<Client> m_clients = new List<Client>();
        private object m_initLock = new object();

        private static readonly TimeSpan StaticFileCacheDuration = new TimeSpan(0, 15, 0);
    }
}
