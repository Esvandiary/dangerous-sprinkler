using System;
using System.Buffers;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net.WebSockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace DangerousSprinkler
{
    public class Client : IDisposable
    {
        public delegate Task HandlerFunction(Client client, string message, JObject content);

        public Client(WebSocket socket, Logger logger)
        {
            m_socket = socket;
            m_logger = logger;
        }


        public async Task SendAsync(string message, JObject content)
        {
            JObject root = new JObject();
            root.Add("message", message);
            root.Add("content", content);
            await SendTextAsync(root.ToString(Formatting.None));
        }

        public async Task<bool> AttemptDispatch(string message)
        {
            try
            {
                JObject o = JObject.Parse(message);
                string msg = o.Value<string>("message");
                if (m_handlers.ContainsKey(msg))
                {
                    await m_handlers[msg](this, msg, o.Value<JObject>("content"));
                    return true;
                }
            }
            catch (Exception ex)
            {
                m_logger.Error($"EXCEPTION in AttemptDispatch: {ex.Message}");
                m_logger.Error($"received data: {message}");
                m_logger.Error($"stack trace: {ex.StackTrace}");
            }
            return false;
        }


        public void SetMessageHandler(string message, HandlerFunction fn)
        {
            m_handlers[message] = fn;
        }

        public void ClearMessageHandler(string message)
        {
            m_handlers.Remove(message);
        }

#region Low-level comms
        public async Task<string> ReceiveTextAsync() => await ReceiveTextAsync(CancellationToken.None);
        public async Task<string> ReceiveTextAsync(CancellationToken token)
        {
            string s = "";
            byte[] buf = ArrayPool<byte>.Shared.Rent(BufferSize);
            while (m_socket != null)
            {
                var result = await m_socket.ReceiveAsync(new ArraySegment<byte>(buf), token);
                if (result.CloseStatus.HasValue || token.IsCancellationRequested) { s = null; break; }
                s += Encoding.UTF8.GetString(buf, 0, result.Count);
                if (result.EndOfMessage) break;
            }
            ArrayPool<byte>.Shared.Return(buf);
            return s;
        }

        public async Task SendTextAsync(string text) => await SendTextAsync(text, CancellationToken.None);
        public async Task SendTextAsync(string text, CancellationToken token)
        {
            byte[] buf = Encoding.UTF8.GetBytes(text);
            await m_socket.SendAsync(new ArraySegment<byte>(buf), WebSocketMessageType.Text, true, token);
        }

        public async Task CloseAsync(string message) => await CloseAsync(message, CancellationToken.None);
        public async Task CloseAsync(string message, CancellationToken token)
        {
            if (m_socket != null && !m_socket.CloseStatus.HasValue)
                await m_socket.CloseAsync(WebSocketCloseStatus.NormalClosure, message, token);
        }
#endregion

#region IDisposable
        // Must ensure double-dispose is harmless since this can get disposed in a few ways
        // and we want to make sure we can afford to catch them all rather than miss one
        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                m_socket?.Dispose();
                m_socket = null;
            }
        }
#endregion

        private WebSocket m_socket;
        private Logger m_logger;
        private IDictionary<string, HandlerFunction> m_handlers = new Dictionary<string, HandlerFunction>();
        public int BufferSize { get; set; } = DefaultBufferSize;

        public const int DefaultBufferSize = 4096;
    }
}