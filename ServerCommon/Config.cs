using System;
using System.IO;
using Newtonsoft.Json.Linq;

namespace DangerousSprinkler
{
    public interface IConfigNamespace
    {
        bool Has(string path);
        T Get<T>(string path);
        T GetOrDefault<T>(string path, T defaultValue);
        void Set<T>(string path, T value);
    }

    public class ConfigSection : IConfigNamespace
    {
        public T Get<T>(string property) => Lookup(property).ToObject<T>();
        public T GetOrDefault<T>(string property, T defaultValue)
        {
            try { return Get<T>(property); } catch (ArgumentException) { return defaultValue; }
        }
        public bool Has(string property)
        {
            try { return Lookup(property) != null; } catch (ArgumentException) { return false; }
        }

        public void Set<T>(string property, T value)
        {
            JObject current = m_root;
            string[] pathParts = property.Split('.');
            try
            {
                for (int i = 0; i < pathParts.Length - 1; ++i)
                {
                    if (!current.ContainsKey(pathParts[i]))
                        current[pathParts[i]] = new JObject();
                    current = current[pathParts[i]].ToObject<JObject>();
                }
                current[pathParts[pathParts.Length-1]] = JToken.FromObject(value);
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"failed to set config property '{property}': {ex.Message}");
            }
        }

        protected JToken Lookup(string property)
        {
            JToken current = m_root;
            string[] pathParts = property.Split('.');
            try
            {
                for (int i = 0; i < pathParts.Length; ++i)
                    current = current.ToObject<JObject>().GetValue(pathParts[i]);
                return current;
            }
            catch (Exception)
            {
                throw new ArgumentException($"failed to read config property '{property}': property does not exist");
            }
        }

        protected ConfigSection(JObject root)
        {
            m_root = root;
        }

        private JObject m_root;
    }

    public class Config : ConfigSection
    {
        public static Config Load(string path)
            => LoadString(File.ReadAllText(path), Path.GetFullPath(path));

        public static Config LoadString(string content, string path = null)
            => new Config(JObject.Parse(content), path);

        private Config(JObject root, string path) : base(root)
        {
            m_path = path;
        }

        private string m_path;
    }
}