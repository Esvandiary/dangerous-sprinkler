namespace DangerousSprinkler
{
    public class CoreServices
    {
        public Config Config { get; init; }
        public Logging Logging { get; init; }
        public Metrics Metrics { get; init; }
    }
}