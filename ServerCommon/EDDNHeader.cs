using System;
using System.Globalization;
using Newtonsoft.Json;

namespace DangerousSprinkler.EDDN
{
    public class Header
    {
        [JsonProperty("uploaderID", Required=Required.Always)]
        public string UploaderID { get; set; }
        [JsonProperty("softwareName", Required=Required.Always)]
        public string SoftwareName { get; set; }
        [JsonProperty("softwareVersion", Required=Required.Always)]
        public string SoftwareVersion { get; set; }
        [JsonProperty("gatewayTimestamp")]
        public string GatewayTimestampText { get; set; }
        [JsonIgnore]
        public DateTimeOffset GatewayTimestamp { get { return DateTimeUtil.FromISO8601(GatewayTimestampText); } }
    }
}