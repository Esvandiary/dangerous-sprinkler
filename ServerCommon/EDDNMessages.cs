using System;
using Newtonsoft.Json;
using DangerousSprinkler;
using DangerousSprinkler.Extensions;

namespace DangerousSprinkler.EDDN
{
    public class CommodityUpdate
    {
        [JsonProperty("systemName")]
        public string SystemName { get; set; }
        [JsonProperty("stationName")]
        public string StationName { get; set; }
        [JsonProperty("marketId")]
        public ulong MarketID { get; set; }
        [JsonProperty("timestamp")]
        public string UpdatedAtText { get; set; }
        [JsonIgnore]
        public DateTimeOffset UpdatedAt { get { return DateTimeUtil.FromISO8601(UpdatedAtText); } }
        [JsonProperty("commodities")]
        public CommodityUpdateEntry[] Commodities { get; set; }
        [JsonProperty("economies")]
        public CommodityUpdateEconomy[] Economies { get; set; }
        [JsonProperty("prohibited")]
        public string[] ProhibitedCommodities { get; set; }
    }

    public class CommodityUpdateEntry
    {
        [JsonProperty("name")]
        public string SymbolicName { get; set; }
        [JsonProperty("meanPrice")]
        public long MeanPrice { get; set; }
        [JsonProperty("buyPrice")]
        public long BuyPrice { get; set; }
        [JsonProperty("stock")]
        public long Supply { get; set; }
        [JsonProperty("stockBracket")]
        public string SupplyBracketText { get; set; }
        [JsonIgnore]
        public CommodityBracket SupplyBracket { get { return CAPIUtil.ParseBracket(SupplyBracketText); } }
        [JsonProperty("sellPrice")]
        public long SellPrice { get; set; }
        [JsonProperty("demand")]
        public long Demand { get; set; }
        [JsonProperty("demandBracket")]
        public string DemandBracketText { get; set; }
        [JsonIgnore]
        public CommodityBracket DemandBracket { get { return CAPIUtil.ParseBracket(DemandBracketText); } }
        [JsonProperty("statusFlags")]
        public string[] StatusFlags { get; set; }
    }

    public class CommodityUpdateEconomy
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("proportion")]
        public double Proportion { get; set; }
    }
}