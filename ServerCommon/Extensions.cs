using System;
using System.Data.Common;
using System.Threading.Tasks;

namespace DangerousSprinkler.Extensions
{
    public static class DbConnectionExtensions
    {
        public static void ExecuteCommand(this DbConnection dbc, string command)
        {
            using (DbCommand cmd = dbc.CreateCommand())
            {
                cmd.CommandText = command;
                cmd.ExecuteNonQuery();
            }
        }

        public static async Task ExecuteCommandAsync(this DbConnection dbc, string command)
        {
            using (DbCommand cmd = dbc.CreateCommand())
            {
                cmd.CommandText = command;
                await cmd.ExecuteNonQueryAsync();
            }
        }
    }

    public static class DbParameterExtensions
    {
        public static void SetValueOrNull(this DbParameter p, object value)
        {
            if (value != null)
                p.Value = value;
            else
                p.Value = DBNull.Value;
        }

        public static void SetValueOrNull<T>(this DbParameter p, T? value) where T : struct
        {
            if (value.HasValue)
                p.Value = value.Value;
            else
                p.Value = DBNull.Value;
        }
    }

    public static class DbDataReaderExtensions
    {
        public static bool GetBoolean(this DbDataReader reader, string column)
            => reader.GetBoolean(reader.GetOrdinal(column));
        public static byte GetByte(this DbDataReader reader, string column)
            => reader.GetByte(reader.GetOrdinal(column));
        public static char GetChar(this DbDataReader reader, string column)
            => reader.GetChar(reader.GetOrdinal(column));
        public static DbDataReader GetData(this DbDataReader reader, string column)
            => reader.GetData(reader.GetOrdinal(column));
        public static DateTime GetDateTime(this DbDataReader reader, string column)
            => reader.GetDateTime(reader.GetOrdinal(column));
        public static decimal GetDecimal(this DbDataReader reader, string column)
            => reader.GetDecimal(reader.GetOrdinal(column));
        public static double GetDouble(this DbDataReader reader, string column)
            => reader.GetDouble(reader.GetOrdinal(column));
        public static Type GetFieldType(this DbDataReader reader, string column)
            => reader.GetFieldType(reader.GetOrdinal(column));
        public static T GetFieldValue<T>(this DbDataReader reader, string column)
            => reader.GetFieldValue<T>(reader.GetOrdinal(column));
        public static float GetFloat(this DbDataReader reader, string column)
            => reader.GetFloat(reader.GetOrdinal(column));
        public static Guid GetGuid(this DbDataReader reader, string column)
            => reader.GetGuid(reader.GetOrdinal(column));
        public static Int16 GetInt16(this DbDataReader reader, string column)
            => reader.GetInt16(reader.GetOrdinal(column));
        public static Int32 GetInt32(this DbDataReader reader, string column)
            => reader.GetInt32(reader.GetOrdinal(column));
        public static Int64 GetInt64(this DbDataReader reader, string column)
            => reader.GetInt64(reader.GetOrdinal(column));
        public static System.IO.Stream GetStream(this DbDataReader reader, string column)
            => reader.GetStream(reader.GetOrdinal(column));
        public static string GetString(this DbDataReader reader, string column)
            => reader.GetString(reader.GetOrdinal(column));
        public static System.IO.TextReader GetTextReader(this DbDataReader reader, string column)
            => reader.GetTextReader(reader.GetOrdinal(column));
        public static object GetValue(this DbDataReader reader, string column)
            => reader.GetValue(reader.GetOrdinal(column));
        public static bool IsDBNull(this DbDataReader reader, string column)
            => reader.IsDBNull(reader.GetOrdinal(column));

        public static DateTimeOffset GetUTCDateTimeOffset(this DbDataReader reader, int ordinal)
            => reader.GetDateTime(ordinal).ToDateTimeOffset(TimeSpan.Zero);
        public static DateTimeOffset GetUTCDateTimeOffset(this DbDataReader reader, string column)
            => reader.GetDateTime(column).ToDateTimeOffset(TimeSpan.Zero);

        public static T GetIdentifier<T>(this DbDataReader reader, int ordinal) where T : struct, Enum
        {
            if (!reader.IsDBNull(ordinal))
                return Identifier.FromNumber<T>(reader.GetInt32(ordinal));
            else
                return Enum.Parse<T>("Unknown");
        }
        public static T GetIdentifier<T>(this DbDataReader reader, string column) where T : struct, Enum
            => GetIdentifier<T>(reader, reader.GetOrdinal(column));

        public static T? Maybe<T>(this DbDataReader reader, int ordinal , Func<int, T> fn) where T : struct
            => (!reader.IsDBNull(ordinal)) ? fn(ordinal) : null;
        public static T? Maybe<T>(this DbDataReader reader, string column, Func<string, T> fn) where T : struct
            => (!reader.IsDBNull(column)) ? fn(column) : null;
        public static T Maybe<T>(this DbDataReader reader, int ordinal, Func<int, T> fn, T defaultValue)
            => (!reader.IsDBNull(ordinal)) ? fn(ordinal) : defaultValue;
        public static T Maybe<T>(this DbDataReader reader, string column, Func<string, T> fn, T defaultValue)
            => (!reader.IsDBNull(column)) ? fn(column) : defaultValue;
    }
}