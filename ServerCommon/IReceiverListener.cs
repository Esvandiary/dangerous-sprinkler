using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;


namespace DangerousSprinkler
{
    public interface IReceiverListener
    {
        ReadOnlyCollection<string> SchemaRefs { get; }
        BlockingCollection<JObject> Queue { get; }
    }
}