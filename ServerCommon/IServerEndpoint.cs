using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace DangerousSprinkler
{
    public interface IServerEndpoint
    {
        string Name { get; }
        string Description { get; }
    }

    public interface IServerWSEndpoint : IServerEndpoint
    {
        ReadOnlyCollection<string> EndpointWSAddresses { get; }
        Task WSClientConnected(Client client, string endpoint);
        Task WSClientDisconnected(Client client, string endpoint);
    }

    public interface IServerHTTPEndpoint : IServerEndpoint
    {
        ReadOnlyCollection<string> EndpointHTTPAddresses { get; }
        Task HTTPRequestReceived(HttpRequest request, HttpResponse response);
    }
}