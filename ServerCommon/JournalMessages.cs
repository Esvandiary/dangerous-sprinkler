using System;
using Newtonsoft.Json;

namespace DangerousSprinkler
{
    public static class JournalMessages
    {
        public class SystemInfo
        {
            [JsonProperty("StarSystem")]
            public string Name { get; set; }
            [JsonProperty("SystemAddress")]
            public ulong ID { get; set; }
            [JsonProperty("StarPos")]
            public double[] Position { get; set; }
            [JsonProperty("SystemAllegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("SystemEconomy")]
            public string PrimaryEconomy { get; set; }
            [JsonProperty("SystemSecondEconomy")]
            public string SecondaryEconomy { get; set; }
            [JsonProperty("SystemGovernment")]
            public string Government { get; set; }
            [JsonProperty("SystemSecurity")]
            public string Security { get; set; }
            [JsonProperty("Population")]
            public long Population { get; set; }
            [JsonProperty("Factions")]
            public SystemFaction[] Factions { get; set; } = new SystemFaction[0];
            [JsonProperty("SystemFaction")]
            public OwnerFaction OwnerFaction { get; set; }
        }

        public class SystemFaction
        {
            [JsonProperty("Name")]
            public string Name { get; set; }
            [JsonProperty("Government")]
            public string Government { get; set; }
            [JsonProperty("Influence")]
            public double Influence { get; set; }
            [JsonProperty("Allegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("Happiness")]
            public string Happiness { get; set; }
            [JsonProperty("ActiveStates")]
            public FactionState[] ActiveStates { get; set; } = new FactionState[0];
            [JsonProperty("PendingStates")]
            public FactionState[] PendingStates { get; set; } = new FactionState[0];
            [JsonProperty("RecoveringStates")]
            public FactionState[] RecoveringStates { get; set; } = new FactionState[0];
        }

        public class OwnerFaction
        {
            [JsonProperty("Name")]
            public string Name { get; set; }
            [JsonProperty("FactionState")]
            public string State { get; set; }
        }

        public class FactionState
        {
            [JsonProperty("State")]
            public string State { get; set; }
            [JsonProperty("Trend")]
            public int? Trend { get; set; }
        }


        public class StationInfo
        {
            [JsonProperty("StationName")]
            public string Name { get; set; }
            [JsonProperty("StationType")]
            public string Type { get; set; }
            [JsonProperty("StarSystem")]
            public string SystemName { get; set; }
            [JsonProperty("SystemAddress")]
            public ulong SystemID { get; set; }
            [JsonProperty("MarketID")]
            public ulong MarketID { get; set; }
            [JsonProperty("StationFaction")]
            public OwnerFaction OwnerFaction { get; set; }
            [JsonProperty("StationAllegiance")]
            public string Allegiance { get; set; }
            [JsonProperty("StationGovernment")]
            public string Government { get; set; }
            [JsonProperty("StationServices")]
            public string[] Services { get; set; } = new string[0];
            [JsonProperty("StationEconomies")]
            public Economy[] Economies { get; set; } = new Economy[0];
            [JsonProperty("DistFromStarLS")]
            public double DistanceFromStar { get; set; }
        }

        public class Economy
        {
            [JsonProperty("Name")]
            public string Name { get; set; }
            [JsonProperty("Proportion")]
            public double Proportion { get; set; }
        }
    }
}