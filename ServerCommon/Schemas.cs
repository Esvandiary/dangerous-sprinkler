using System;

namespace DangerousSprinkler
{
    public static class SchemaURLs
    {
        public const string BlackMarket_v10 = "https://eddn.edcd.io/schemas/blackmarket/1";
        public const string Commodity_v30 = "https://eddn.edcd.io/schemas/commodity/3";
        public const string Journal_v10 = "https://eddn.edcd.io/schemas/journal/1";
        public const string Outfitting_v20 = "https://eddn.edcd.io/schemas/outfitting/2";
        public const string Shipyard_v20 = "https://eddn.edcd.io/schemas/shipyard/2";
    }

    public static class CAPIUtil
    {
        public static CommodityBracket ParseBracket(string bracket)
        {
            switch (bracket)
            {
            case "0": case "1": case "2": case "3":
                return Enum.Parse<CommodityBracket>(bracket);
            case "":
                return CommodityBracket.TemporaryAvailability;
            default:
                throw new ArgumentException($"failed to parse commodity bracket from input \"{bracket}\"");
            }
        }
    }
}