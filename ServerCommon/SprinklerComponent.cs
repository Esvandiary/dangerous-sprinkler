using System;
using System.Data.Common;
using DangerousSprinkler.Extensions;
using Microsoft.Extensions.Logging;

namespace DangerousSprinkler
{
    public interface IDBDeriveParameters
    {
        void DeriveParameters(DbCommand command);
    }

    public abstract class SprinklerComponent : IDisposable
    {
        public SprinklerComponent(CoreServices cs, string id, string name)
        {
            CoreServices = cs;
            ComponentID = id;
            ComponentName = name;
            Logger = CoreServices.Logging.GetLogger(ComponentID);
        }

        protected CoreServices CoreServices { get; private set; }
        protected Config Config { get => CoreServices.Config; }
        protected Logger Logger { get; private set; }
        protected Metrics Metrics { get => CoreServices.Metrics; }

        protected Logger GetSubLogger(string id)
            => CoreServices.Logging.GetLogger($"{ComponentID}.{id}");

        public string ComponentID { get; private set; }
        public string ComponentName { get; private set; }
        public virtual void Initialise() {}
        public virtual void Start() { IsRunning = true; }
        public virtual void Stop() { IsRunning = false; }
        public bool IsRunning { get; private set; } = false;

        public void Dispose() { Dispose(true); }
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
                Stop();
        }

        public Status GetStatus(DbConnection dbc, IDBDeriveParameters dbm)
        {
            using (var cmd = dbc.CreateCommand())
            {
                cmd.CommandText = "SELECT initialised_at, updated_at FROM components WHERE id = @id";
                dbm.DeriveParameters(cmd);
                cmd.Parameters["@id"].Value = ComponentID;
                var reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    return new Status {
                        InitialisedAt = reader.Maybe("initialised_at", reader.GetUTCDateTimeOffset),
                        UpdatedAt = reader.Maybe("updated_at", reader.GetUTCDateTimeOffset)
                    };
                }
                else
                {
                    return new Status();
                }
            }
        }

        protected void SetComponentInitialised(DbConnection dbc, IDBDeriveParameters dbm, DateTime? initialised_at, DateTime? updated_at)
        {
            using (var component_cmd = dbc.CreateCommand())
            {
                component_cmd.CommandText = "INSERT INTO components VALUES (@id, @itime, @utime)";
                dbm.DeriveParameters(component_cmd);
                component_cmd.Parameters["@id"].Value = ComponentID;
                component_cmd.Parameters["@itime"].SetValueOrNull(initialised_at);
                component_cmd.Parameters["@utime"].SetValueOrNull(updated_at);
                component_cmd.ExecuteNonQuery();
            }
        }

        protected void SetInitialisedAt(DbConnection dbc, IDBDeriveParameters dbm, DateTime? time)
            => SetTime(dbc, dbm, "initialised_at", time);
        protected void SetUpdatedAt(DbConnection dbc, IDBDeriveParameters dbm, DateTime? time)
            => SetTime(dbc, dbm, "updated_at", time);

        private void SetTime(DbConnection dbc, IDBDeriveParameters dbm, string column, DateTime? time)
        {
            using (var component_cmd = dbc.CreateCommand())
            {
                component_cmd.CommandText = $"UPDATE components SET {column} = @time WHERE id = @id";
                dbm.DeriveParameters(component_cmd);
                component_cmd.Parameters["@time"].SetValueOrNull(time);
                component_cmd.Parameters["@id"].Value = ComponentID;
                component_cmd.ExecuteNonQuery();
            }
        }

        public class Status
        {
            public DateTimeOffset? InitialisedAt { get; set; }
            public DateTimeOffset? UpdatedAt { get; set; }
        }
    }
}