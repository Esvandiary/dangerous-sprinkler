using Newtonsoft.Json;

namespace DangerousSprinkler
{
    public class CommodityEntry
    {
        [JsonProperty("id")]
        public ulong ID { get; set; }
        [JsonProperty("symbolic_name")]
        public string SymbolicName { get; set; }
        [JsonProperty("category_name")]
        public string Category { get; set; }
        [JsonProperty("localised_name")]
        public string LocalisedName { get; set; }
        [JsonProperty("rare_market_id")]
        public ulong? MarketID { get; set; }
        [JsonIgnore]
        public bool IsRare { get { return MarketID.HasValue; } }
    }
}