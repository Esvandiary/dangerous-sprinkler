using System;
using Newtonsoft.Json.Linq;

namespace DangerousSprinkler
{
    public class JSONWSClient : WSClient
    {
        public event EventHandler<MessageReceivedEventArgs> MessageReceived;
        public JSONWSClient(Uri addr) : base(addr) {}
        protected override void HandleRawMessage(string message)
        {
            try
            {
                JObject msg = JObject.Parse(message);
                var args = new MessageReceivedEventArgs {
                    Message = msg.Value<string>("message"),
                    Content = msg.Value<JObject>("content")
                };
                MessageReceived?.Invoke(this, args);
            }
            catch (Exception)
            {
                // TODO: log somehow
            }
        }

        public class MessageReceivedEventArgs : EventArgs
        {
            public string Message { get; init; }
            public JObject Content { get; init; }
        }
    }
}