using System;
using Newtonsoft.Json;

namespace DangerousSprinkler
{
    public class LiveCommodityUpdate
    {
        [JsonProperty("commodity_name")]
        public string CommodityName { get; set; }
        [JsonProperty("above_min_price")]
        public bool AboveMinPrice { get; set; }
        [JsonProperty("system_name")]
        public string SystemName { get; set; }
        [JsonProperty("system_id")]
        public ulong SystemID { get; set; }
        [JsonProperty("distance_to_ref")]
        public double DistanceToRefSystem { get; set; }
        [JsonProperty("station_name")]
        public string StationName { get; set; }
        [JsonProperty("station_type")]
        public string StationType { get; set; }
        [JsonProperty("market_id")]
        public ulong MarketID { get; set; }
        [JsonProperty("max_pad_size")]
        public string MaxPadSize { get; set; }
        [JsonProperty("sc_distance")]
        public ulong DistanceFromStar { get; set; }
        [JsonProperty("sell_price")]
        public long SellPrice { get; set; }
        [JsonProperty("demand")]
        public long Demand { get; set; }
        [JsonProperty("demand_bracket")]
        public string DemandBracket { get; set; }
        [JsonProperty("updated_at")]
        public string UpdatedAt { get; set; }

        public LiveCommodityUpdate Clone(bool aboveMin, double distToRef)
        {
            var update = (LiveCommodityUpdate)MemberwiseClone();
            update.AboveMinPrice = aboveMin;
            update.DistanceToRefSystem = distToRef;
            return update;
        }
    }

    public class LiveCommoditySubscriptionInfo
    {
        [JsonProperty("commodity_name")]
        public string Name { get; set; }
        [JsonProperty("min_price")]
        public long MinPrice { get; set; }
        [JsonProperty("ref_system")]
        public string ReferenceSystemName { get; set; }
        [JsonProperty("ref_max_distance")]
        public double MaxDistance { get; set; }
        [JsonProperty("max_sc_distance")]
        public long MaxSCDistance { get; set; }
    }
}