﻿using System;
using System.Collections.Concurrent;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using DangerousSprinkler.EDDN;
using Newtonsoft.Json.Linq;

namespace DangerousSprinkler
{
    public class StationTracker : SprinklerComponent, IServerWSEndpoint, IReceiverListener
    {
        public StationTracker(CoreServices cs, DBManager db, GalaxyInfo gi)
            : base(cs, "StationTracker", "Station Tracker")
        {
            m_db = db;
            m_gi = gi;

            m_processingThread = new Thread(RunProcessingThread);
        }

#region IReceiverListener
        public ReadOnlyCollection<string> SchemaRefs { get; } = new(new[]{ SchemaURLs.Journal_v10, SchemaURLs.Commodity_v30 });
        public BlockingCollection<JObject> Queue { get; } = new();
#endregion

#region SprinklerComponent
        public override void Start()
        {
            base.Start();
            m_processingThread.Start();
        }

        public override void Stop()
        {
            base.Stop();
            if (m_processingThread.IsAlive)
                m_processingThread.Join();
        }
#endregion

#region IServerWSEndpoint
        public ReadOnlyCollection<string> EndpointWSAddresses { get; } = new ReadOnlyCollection<string>(new[]{ "/api/stations" });
        public string Name { get; } = "Station Updates";
        public string Description { get; } = "Provides updates on station changes";

        public Task WSClientConnected(Client client, string endpoint)
        {
            Logger.Debug("client connected");
            // client.SetMessageHandler("config", HandleConfigMessage);
            // client.SetMessageHandler("subscribe", HandleSubscribeMessage);
            // client.SetMessageHandler("unsubscribe", HandleUnubscribeMessage);
            return Task.CompletedTask;
        }

        public Task WSClientDisconnected(Client client, string endpoint)
        {
            Logger.Debug("client disconnected");
            // foreach (var list in m_liveSubscriptions.Values)
            //     list.RemoveAll(t => t.Client == client);
            return Task.CompletedTask;
        }
#endregion

        private void RunProcessingThread()
        {
            while (IsRunning)
            {
                try
                {
                    JObject o = null;
                    while (IsRunning && !Queue.TryTake(out o, 250));
                    if (!IsRunning) break;
                    if (o.Value<string>("$schemaRef") == SchemaURLs.Journal_v10)
                    {
                        if (o.ContainsKey("message") && o.Value<JObject>("message").ContainsKey("event"))
                        {
                            JObject msg = o.Value<JObject>("message");
                            string evt = msg.Value<string>("event");
                            switch (msg.Value<string>("event"))
                            {
                                case "FSDJump":
                                case "Location":
                                case "Docked":
                                    HandleSystemStationUpdate(msg);
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                    else if (o.Value<string>("$schemaRef") == SchemaURLs.Commodity_v30)
                    {
                        CommodityUpdate u = o["message"].ToObject<CommodityUpdate>();
                        HandleCommodityUpdate(u);
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error($"EXCEPTION processing journal message: {ex.Message}");
                    Logger.Error($"Stack trace: {ex.StackTrace}");
                }
            }
        }

        private void HandleSystemStationUpdate(JObject msg)
        {
            string evt = msg.Value<string>("event");
            DateTimeOffset timestamp = DateTimeUtil.FromISO8601(msg.Value<string>("timestamp"));
            bool hasSystemInfo = (evt == "FSDJump" || evt == "Location");
            bool hasStationInfo = (evt == "Docked" || (evt == "Location" && msg.Value<bool>("Docked")));

            // TODO: notify subscriptions of station changes
        }

        private void HandleCommodityUpdate(CommodityUpdate u)
        {
            // TODO: notify subscriptions of market update
        }

        private DBManager m_db;
        private GalaxyInfo m_gi;
        private Thread m_processingThread;
    }
}
